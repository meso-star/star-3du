# Star 3D shapes utilities

This library creates *Star-3D* predefined shapes from parameters.

## How to build

This library depends on the
[CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/#tab-readme) package to build and
depends on the [RSys](https://gitlab.com/vaplv/rsys/#tab-readme) library.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the RSys, and Star-3D libraries. Finally
generate the project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of the previously defined
dependencies. The generated project can be edited, built, tested and installed
as any CMake project.

## License

*Star-3DU* is Copyright (C) |Meso|Star> 2015 (<contact@meso-star.com>). It
is a free software released under the [OSI](http://opensource.org)-approved
CeCILL license. You are welcome to redistribute it under certain conditions;
refer to the COPYING files for details.


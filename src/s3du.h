/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef S3DU_H
#define S3DU_H

#include <rsys/rsys.h>
#include <float.h>

/* Library symbol management */
#if defined(S3DU_SHARED_BUILD) /* Build shared library */
  #define S3DU_API extern EXPORT_SYM
#elif defined(S3DU_STATIC) /* Use/build static library */
  #define S3DU_API extern LOCAL_SYM
#else /* Use shared library */
  #define S3DU_API extern IMPORT_SYM
#endif

/* Forward declaration of external data types */
struct s3d_device;
struct s3d_shape;

/*******************************************************************************
 * Helper types
 ******************************************************************************/

/* the type for mesh orientation */
enum mesh_orientation_T { CW, CCW };

/* the type for rotation */
enum rotation_T { LEVO, DEXTRO };

/* type for tube terminations */
enum termination_T { NONE, DISK, SPHERE };

/* type of spheres */
enum sphere_type_T { ICOSPHERE, GEODESIC };

/* mesh refinement policy */
enum mesh_refinement_policy_T { UNIFORM, ADAPTATIVE };

/* type of parametrization used */
enum parametrization_T {
  /* theta in [0 pi] and phi in [0 2pi] */
  SPHERICAL_1,
  /* theta in [-pi/2 +pi/2] and phi in [-pi +pi] */
  SPHERICAL_2,
  UNDEF
};

/*
 the type of a parametric function
 given the 2 parameters, the function fills pt up and returns its distance from origin

 2 parametrization are implemented:
 SPHERICAL_1: theta in [0 pi] and phi in [0 2pi]
 SPHERICAL_2: theta in [-pi/2 +pi/2] and phi in [-pi +pi]
*/
typedef float(*param_fn_T)(float param1, float param2, float pt[3]);

/* type for s3du_shape_mesh_param_function options */
struct param_mesh_options_T {
  /* center */
  float center[3];
  /*
   level of subdivision
   at level 0 the mesh has 80 triangles
   the maximum increase factor is 4, the typical one is 2
  */
  uint32_t level;
  /* mesh refinement policy */
  enum mesh_refinement_policy_T refinement_policy;
  /* parametrization */
  enum parametrization_T convention;
  /* mesh orientation */
  enum mesh_orientation_T mesh_orientation;
  /* the type of underlying sphere */
  enum sphere_type_T base_sphere_type;
  /*
   relative error target:
   max distance between the mesh and the function
   relative to the mesh size (in ]0 1[)
  */
  float relative_error;
  /* minimum size of edges, relative to bounding sphere's circumference */
  float relative_min_edge;
  /* minimum surface of triangles, relative to bounding sphere's surface */
  float relative_min_surf;
  /* edge sampling parameters */
  uint32_t initial_samples_per_radian;
  uint32_t final_samples_per_radian;
  /* output the final mesh (obj file)? */
  char output_final_mesh;
  /* output refinement steps (vtk files)? */
  char output_refinement_steps;
  /* base name for the output files */
  const char* output_files_base_name;
};

/* type for make_spherical_mesh options */
struct spherical_mesh_options_T {
  /* center */
  float center[3];
  /* the type of sphere */
  enum sphere_type_T type;
  /*
   level of subdivision
   for icospheres: 80*4^level triangles
   for geodesic spheres: (8 + 2 * level)� triangles
  */
  uint32_t level;
  /* mesh orientation */
  enum mesh_orientation_T mesh_orientation;
  /* output the final mesh (obj file)? */
  char output_final_mesh;
  /* base name for the output file */
  const char* output_files_base_name;
};

/* type for make_helicoidal_mesh options */
struct helicoidal_mesh_options_T {
  /* center */
  float center[3];
  /*
  level of subdivision
  at level 0 the mesh has 80 triangles
  the maximum increase factor is 4, the typical one is 2
  */
  uint32_t level;
  /* mesh orientation */
  enum mesh_orientation_T mesh_orientation;
  /* rotation */
  enum rotation_T rotation;
  /* terminations */
  enum termination_T termination;
  /* angle offset */
  float starting_angle;
  /* output the final mesh (obj file)? */
  char output_final_mesh;
  /* base name for the output file */
  const char* output_files_base_name;
};

/* type for s3du_shape_create_meshed_parallelepiped options */
struct meshed_parallelepiped_options_T {
  /* center */
  float center[3];
  /* mesh orientation */
  enum mesh_orientation_T mesh_orientation;
  /* output the final mesh (obj file)? */
  char output_final_mesh;
  /* base name for the output file */
  const char* output_files_base_name;
};

BEGIN_DECLS

S3DU_API const struct param_mesh_options_T default_mesh_param_function_options;
S3DU_API const struct spherical_mesh_options_T default_spherical_mesh_options;
S3DU_API const struct helicoidal_mesh_options_T default_helicoidal_mesh_options;
S3DU_API const struct meshed_parallelepiped_options_T default_meshed_parallelepiped_options;

S3DU_API const float default_center[3];
S3DU_API const uint32_t default_level;
S3DU_API const enum parametrization_T default_parametrization;
S3DU_API const float default_relative_error;
S3DU_API const float default_relative_min_edge;
S3DU_API const float default_relative_min_surf;
S3DU_API const uint32_t default_initial_samples_per_radian;
S3DU_API const uint32_t default_final_samples_per_radian;
S3DU_API const enum mesh_orientation_T default_mesh_orientation;
S3DU_API const enum rotation_T default_rotation;
S3DU_API const enum termination_T default_termination;
S3DU_API const float default_starting_angle;
S3DU_API const char default_output_final_mesh;
S3DU_API const char default_output_refinement_steps;
S3DU_API const char* const default_output_files_base_name;
S3DU_API const enum sphere_type_T default_sphere_type;
S3DU_API const enum mesh_refinement_policy_T default_refinement_policy;

/*******************************************************************************
 * Shape utility API - Define specific shapes
 ******************************************************************************/

/*
 create a parallelepiped
*/
S3DU_API res_T
s3du_shape_create_meshed_parallelepiped
  (struct s3d_device* dev,
   struct s3d_shape** shape,
   const float edge1[3],
   const float edge2[3],
   const float edge3[3],
   const struct meshed_parallelepiped_options_T* options);

/*
 create a sphere
*/
S3DU_API res_T
s3du_shape_create_sphere
  (struct s3d_device* dev,
   struct s3d_shape** out_pp,
   float radius,
   const struct spherical_mesh_options_T* options);

/*
 create a shape from a parametric function
*/
S3DU_API res_T
s3du_shape_mesh_param_function
  (struct s3d_device* dev,
   struct s3d_shape** out_is,
   param_fn_T param_fn,
   const struct param_mesh_options_T* options);

/*
 create an helicoidal shape
*/
S3DU_API res_T
s3du_shape_create_helicoid
  (struct s3d_device* dev,
   struct s3d_shape** out_pp,
   float H,
   float P,
   float D,
   float d,
   const struct helicoidal_mesh_options_T* _options);

END_DECLS

#endif /* S3DU_H */


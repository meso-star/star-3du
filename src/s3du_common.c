/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use,
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited
* liability.
*
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security.
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms. */

#include "s3du_common.h"

#include <rsys/rsys.h>
#include <rsys/float3.h>

#include <limits>
#define _USE_MATH_DEFINES
#include <math.h>
#include <fstream>
#include <iostream>
#include<algorithm>
#include <string>
#include <time.h>

using std::max;
using std::min;
using std::string;
using std::numeric_limits;
using std::ofstream;
using std::endl;
using std::cos;
using std::sin;
using std::asin;
using std::acos;
using std::atan;
using std::atan2;
using std::sqrt;
using std::fmod;

/******************************************************************************
* default values for options
******************************************************************************/

const float default_center[3] = { 0, 0, 0 };
const uint32_t default_level = 2;
const enum parametrization_T default_parametrization = SPHERICAL_1;
const float default_relative_error = 0.0001f;
const float default_relative_min_edge = 0.01f;
const float default_relative_min_surf = 0.001f;
const uint32_t default_initial_samples_per_radian = 72; // 10 sample per �  573 286 143 72
const uint32_t default_final_samples_per_radian = 14324; // 1000 sample per �  57296 28648 14324 7162
const enum mesh_orientation_T default_mesh_orientation = CW;
const enum rotation_T default_rotation = DEXTRO;
const float default_starting_angle = 0;
const enum termination_T default_termination = DISK;
const char default_output_final_mesh = 0;
const char default_output_refinement_steps = 0;
const char* const default_output_files_base_name = "%Y-%m-%d_%H-%M-%S_";
const enum sphere_type_T default_sphere_type = GEODESIC;
const enum mesh_refinement_policy_T default_refinement_policy = ADAPTATIVE;

/******************************************************************************
* common functions
******************************************************************************/

void
_get_ids(const unsigned itri, unsigned ids[3], void* data)
{
  const user_data_T* udata = (user_data_T *) data;
  const vector<trg_T>* triangles = udata->trgs;
#ifndef NDEBUG
  const vector<vrtx_T>* vertices = udata->vrtcs;
#endif
  ASSERT(itri < triangles->size());
  for (int i = 0; i < 3; i++) {
    ids[i] = triangles->operator[](itri).v[i];
    ASSERT(ids[i] < vertices->size());
  }
}

void
_get_position(const unsigned ivert, float position[3], void* data)
{
  const user_data_T* udata = (user_data_T *) data;
  const vector<vrtx_T>*vertices = udata->vrtcs;
  ASSERT(ivert < vertices->size());
  for (int i = 0; i < 3; i++) {
    position[i] = vertices->operator[](ivert).pos[i];
  }
}

void
add_trg(std::vector<trg_T> * triangles, const trg_T& t) {
#ifndef NDEBUG
  // check that the triangle is really a new one
  for (size_t i = 0; i < triangles->size(); i++) {
    const trg_T& trg = triangles->operator[](i);
    uint32_t vv1 = min(trg.v[0], min(trg.v[1], trg.v[2]));
    uint32_t vv2 = max(trg.v[0], min(trg.v[1], trg.v[2]));
    uint32_t vv3 = max(trg.v[0], max(trg.v[1], trg.v[2]));
    uint32_t tv1 = min(t.v[0], min(t.v[1], t.v[2]));
    uint32_t tv2 = max(t.v[0], min(t.v[1], t.v[2]));
    uint32_t tv3 = max(t.v[0], max(t.v[1], t.v[2]));
    ASSERT(vv1 != tv1 || vv2 != tv2 || vv3 != tv3);
  }
#endif
  triangles->push_back(t);
}

void
add_vertex(std::vector<vrtx_T>* v, const vrtx_T& pt) {
#ifndef NDEBUG
  // check that the point is really a new one
  for (size_t i = 0; i < v->size(); i++) {
    const float* pos = v->operator[](i).pos;
    float ddd[3];
    f3_sub(ddd, pos, pt.pos);
    float d2 = f3_dot(ddd, ddd);
    ASSERT(d2 > 1e-12);
  }
#endif
  v->push_back(pt);
}

#ifdef NDEBUG
void
check_convention(const vrtx_T&, parametrization_T) {}

void
check_convention(float, float, parametrization_T) {}
#else
void
check_convention(float p1, float phi, parametrization_T convention) {
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    ASSERT(0 <= p1 && p1 <= (float) M_PI);
    ASSERT(0 <= phi && phi <= (float) (2 * M_PI));
    break;
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    ASSERT(-0.5f * (float) M_PI <= p1 && p1 <= +0.5f * (float) M_PI);
    ASSERT(-(float) M_PI <= phi && phi <= +(float) M_PI);
    break;
  }
  default: ASSERT(false);
  }
}

void
check_convention(const vrtx_T& pt, parametrization_T convention) {
  check_convention(pt.theta, pt.phi, convention);
}
#endif

// just fills dest from pt's p1,phi according to convention
void
populate_cartesian(float *dest, const vrtx_T& pt, parametrization_T convention) {
  populate_cartesian(dest, pt, pt.r, convention);
}

void
populate_cartesian(float *dest, const vrtx_T& pt, float r, parametrization_T convention) {
  check_convention(pt, convention);
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    dest[0] = r * sin(pt.theta) * cos(pt.phi);
    dest[1] = r * sin(pt.theta) * sin(pt.phi);
    dest[2] = r * cos(pt.theta);
    break;
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    dest[0] = r * cos(pt.theta) * cos(pt.phi);
    dest[1] = r * cos(pt.theta) * sin(pt.phi);
    dest[2] = r * sin(pt.theta);
    break;
  }
  default: ASSERT(false);
  }
}

// compute the point radius and cartesian coordinates
// from its 2 angles and the param_fn
void
compute_vertex(vrtx_T* pt, param_fn_T param_fn)
{
  pt->r = param_fn(pt->theta, pt->phi, pt->pos);
}

// clamp p1 angle according to convention
// p1 doesn't loop: here clamp just mean clamp
float
clamp_theta(float theta, parametrization_T convention) {
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    return min(max(theta, 0.0f), (float) M_PI);
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    return min(max(theta, -0.5f * (float) M_PI), 0.5f * (float) M_PI);
  }
  default: {
    ASSERT(false);
    return theta;
  }
  }
}

// clamp phi angle according to convention
// phi loops: clamp mean some sort of mod
float
clamp_phi(float phi, parametrization_T convention) {
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    return fmod((float) (2 * M_PI) + phi, (float) (2 * M_PI));
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    return fmod((float) (3 * M_PI) + phi, (float) (2 * M_PI)) - (float) M_PI;
  }
  default: {
    ASSERT(false);
    return phi;
  }
  }
}

// compute angles from pos and radius according to convention
// don't use the actual radius but the provided one
// can be used with a fake (not-zero) radius
void
get_angles(const float pos[3], float r, float * p1, float * phi, parametrization_T convention) {
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    *p1 = acos(pos[2] / r);
    *phi = clamp_phi(atan2(pos[1], pos[0]), convention);
    break;
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    *p1 = asin(pos[2] / r);
    *phi = clamp_phi(atan2(pos[1], pos[0]), convention);
    break;
  }
  default: ASSERT(false);
  }
  check_convention(*p1, *phi, convention);
}

// compute the vertex between pt1 and pt2 that is located at a given fraction
// return the radius of this point (distance to the pt1-pt2 segment)
// and fills middle up (partially if no param_fn is provided)
float
middle_angles
  (const vrtx_T& pt1,
   const vrtx_T& pt2,
   float fraction,
   param_fn_T param_fn,
   vrtx_T* middle,
   parametrization_T convention)
{
  ASSERT(0 < fraction && fraction < 1);
  ASSERT(middle);
  check_convention(pt1, convention);
  check_convention(pt2, convention);

  float segment_r = 0;
  float to1[3], to2[3], tomid[3];

  // use the 2 vectors from the center to the points
  // build the middle position and go back to angles
  populate_cartesian(to1, pt1, 1, convention);
  populate_cartesian(to2, pt2, 1, convention);

  if (pt1.r > 1e-6 && pt2.r > 1e-6) {
    // this doesn't work if the origin is on/close to the [pt1 pt2] line
    // this test only detects the case where one point is close to the origin
    // the 1e-6 threshold is a complete guess 
    for (int i = 0; i < 3; i++) {
      tomid[i] = pt1.r * (1 - fraction) * to1[i] + pt2.r * fraction * to2[i];
      segment_r += tomid[i] * tomid[i];
    }
    segment_r = sqrt(segment_r);
    get_angles(tomid, segment_r, &middle->theta, &middle->phi, convention);
  }
  else {
    // use the unity sphere to compute angles
    for (int i = 0; i < 3; i++) {
      tomid[i] = (1 - fraction) * to1[i] + fraction * to2[i];
    }
    segment_r = (pt1.r + pt2.r) * 0.5f;
    get_angles(tomid, 1, &middle->theta, &middle->phi, convention);
  }

  // compute radius and position
  if (param_fn) {
    compute_vertex(middle, param_fn);
  }

  return segment_r;
}

void
create_geodesic_sphere
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float radius,
   uint32_t level,
   enum mesh_orientation_T orientation,
   enum parametrization_T convention)
{
  const uint32_t nb_ring = GEO_NB_RINGS(level);
  const uint32_t nb_slice = GEO_NB_SLICES(level);
  const float dphi = (float) (2 * M_PI) / (float) nb_slice;
  const float dtheta = (float) M_PI / (float) (nb_ring - 1);
  uint32_t v[3];
  // CW or CCW?
  int i0 = 0;
  int i1 = (orientation == CCW) ? 1 : 2;
  int i2 = 3 - i1;
  vrtx_T pt;
  pt.r = radius;

  float first_theta, last_theta, first_phi;
  switch (convention) {
  default: ASSERT(false);
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    first_theta = 0;
    last_theta = (float) M_PI;
    first_phi = 0;
    break;
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    first_theta = -0.5f * (float) M_PI;
    last_theta = +0.5f * (float) M_PI;
    first_phi = -(float) M_PI;
    break;
  }
  }

  for (uint32_t ring = 0; ring < nb_ring; ring++) {
    uint32_t starting_idx = (uint32_t) vertices->size();
    // create ring's vertices
    if (ring == 0) {
      // single vertex
      pt.theta = first_theta;
      pt.phi = first_phi;
      populate_cartesian(pt.pos, pt, convention);
      add_vertex(vertices, pt);
      // no previous ring to link to
      continue;
    }
    else if (ring == nb_ring - 1) {
      // single vertex
      pt.theta = last_theta;
      pt.phi = first_phi;
      populate_cartesian(pt.pos, pt, convention);
      add_vertex(vertices, pt);
    }
    else {
      // many vertices
      pt.theta = first_theta + (float) ring * dtheta;
      for (uint32_t slice = 0; slice < nb_slice; slice++) {
        pt.phi = first_phi + (float) slice * dphi;
        populate_cartesian(pt.pos, pt, convention);
        add_vertex(vertices, pt);
      }
    }
    // create ring's triangles
    if (ring == 0) {
      // no previous ring to link to
      continue;
    }
    else if (ring == 1) {
      // connect pole to first ring
      v[i0] = 0;
      for (uint32_t i = 0; i < nb_slice; i++) {
        v[i1] = 1 + (i + 1) % nb_slice; v[i2] = 1 + i;
        add_trg(triangles, trg_T(v));
      }
    }
    else if (ring == nb_ring - 1) {
      // connect last ring to pole
      uint32_t prev_slice = starting_idx - nb_slice;
      v[i2] = starting_idx;
      for (uint32_t i = 0; i < nb_slice; i++) {
        v[i0] = prev_slice + i; v[i1] = prev_slice + (i + 1) % nb_slice;
        add_trg(triangles, trg_T(v));
      }
    }
    else {
      // connect ring to ring
      uint32_t prev_slice = starting_idx - nb_slice;
      for (uint32_t i = 0; i < nb_slice; i++) {
        v[i0] = prev_slice + i; v[i1] = prev_slice + (i + 1) % nb_slice; v[i2] = starting_idx + (i + 1) % nb_slice;
        add_trg(triangles, trg_T(v));
        v[i0] = starting_idx + (i + 1) % nb_slice; v[i1] = starting_idx + i; v[i2] = prev_slice + i;
        add_trg(triangles, trg_T(v));
      }
    }
  }
}

// create an icosphere centered at 0,0,0
// and displace the points using param_fn
void
create_icosphere_0
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float radius,
   enum mesh_orientation_T orientation,
   enum parametrization_T convention)
{
  /*
  The locations of the vertices of a regular icosahedron can be described
  using spherical coordinates, for instance as latitude and longitude.
  If two vertices are taken to be at the north and south poles (latitude �90�),
  then the other ten vertices are at latitude �arctan(1/2) = �26.57� env.
  These ten vertices are at evenly spaced longitudes (36� apart),
  alternating between north and south latitudes.
  */
  float l = atan(0.5f);
  vrtx_T pt;
  pt.r = radius;
  
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    pt.phi = 0;
    pt.theta = 0;
    populate_cartesian(pt.pos, pt, convention);
    add_vertex(vertices, pt);

    pt.theta = (float) M_PI;
    populate_cartesian(pt.pos, pt, convention);
    add_vertex(vertices, pt);

    for (int i = 0; i < 10; i++) {
      pt.theta = (float) (+0.5 * M_PI) + (i % 2 ? +l : -l);
      pt.phi = (float) ((0.2 * i) * M_PI);
      populate_cartesian(pt.pos, pt, convention);
      add_vertex(vertices, pt);
    }
    break;
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    pt.theta = +0.5f * (float) M_PI;
    pt.phi = 0;
    populate_cartesian(pt.pos, pt, convention);
    add_vertex(vertices, pt);

    pt.theta = -0.5f * (float) M_PI;
    populate_cartesian(pt.pos, pt, convention);
    add_vertex(vertices, pt);

    for (int i = 0; i < 10; i++) {
      pt.theta = i % 2 ? -l : +l;
      pt.phi = (float) ((0.2 * i - 1) * M_PI);
      populate_cartesian(pt.pos, pt, convention);
      add_vertex(vertices, pt);
    }
    break;
  }
  default: ASSERT(false);
  }

  // create the 20 triangles of the icosahedron, CW
  uint32_t v[3];
  // CW or CCW?
  int i0 = 0;
  int i1 = (orientation == CCW) ? 1 : 2;
  int i2 = 3 - i1;
  
  // 5 faces around point 0
  v[i0] = 0; v[i1] = 2; v[i2] = 4; add_trg(triangles, trg_T(v));
  v[i0] = 0; v[i1] = 4; v[i2] = 6; add_trg(triangles, trg_T(v));
  v[i0] = 0; v[i1] = 6; v[i2] = 8; add_trg(triangles, trg_T(v));
  v[i0] = 0; v[i1] = 8; v[i2] = 10; add_trg(triangles, trg_T(v));
  v[i0] = 0; v[i1] = 10; v[i2] = 2; add_trg(triangles, trg_T(v));
  
  // 10 faces around equator
  v[i0] = 2; v[i1] = 3; v[i2] = 4; add_trg(triangles, trg_T(v));
  v[i0] = 4; v[i1] = 5; v[i2] = 6; add_trg(triangles, trg_T(v));
  v[i0] = 6; v[i1] = 7; v[i2] = 8; add_trg(triangles, trg_T(v));
  v[i0] = 8; v[i1] = 9; v[i2] = 10; add_trg(triangles, trg_T(v));
  v[i0] = 10; v[i1] = 11; v[i2] = 2; add_trg(triangles, trg_T(v));

  v[i0] = 3; v[i1] = 5; v[i2] = 4; add_trg(triangles, trg_T(v));
  v[i0] = 5; v[i1] = 7; v[i2] = 6; add_trg(triangles, trg_T(v));
  v[i0] = 7; v[i1] = 9; v[i2] = 8; add_trg(triangles, trg_T(v));
  v[i0] = 9; v[i1] = 11; v[i2] = 10; add_trg(triangles, trg_T(v));
  v[i0] = 11; v[i1] = 3; v[i2] = 2; add_trg(triangles, trg_T(v));

  // 5 faces around point 1
  v[i0] = 1; v[i1] = 5; v[i2] = 3; add_trg(triangles, trg_T(v));
  v[i0] = 1; v[i1] = 7; v[i2] = 5; add_trg(triangles, trg_T(v));
  v[i0] = 1; v[i1] = 9; v[i2] = 7; add_trg(triangles, trg_T(v));
  v[i0] = 1; v[i1] = 11; v[i2] = 9; add_trg(triangles, trg_T(v));
  v[i0] = 1; v[i1] = 3; v[i2] = 11; add_trg(triangles, trg_T(v));
}

// refine every triangle of a mesh (diadic refinement)
// can use a function to evaluate radius if provided
// or a constant radius
void
one_step_diadic_refine
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   parametrization_T convention,
   param_fn_T param_fn,
   float radius)
{
  vector<trg_T> new_triangles;
  map<key_T, uint32_t> middle_points;
  for (uint32_t t = 0; t < (uint32_t) triangles->size(); t++) {
    trg_T& tr = triangles->operator[](t);
    uint32_t idx[3];
    for (int i = 0; i < 3; i++) {
      uint32_t p1 = tr.v[i];
      uint32_t p2 = tr.v[(i + 1) % 3];
      key_T key = make_key(p1, p2);
      auto it = middle_points.find(key);
      if (it != middle_points.end()) {
        // allready splited
        idx[i] = it->second;
        ASSERT(idx[i] != std::numeric_limits<uint32_t>::max());
        continue;
      }
      vrtx_T new_vertex;
      new_vertex.r = radius;
      const vrtx_T& pt1 = vertices->operator[](p1);
      const vrtx_T& pt2 = vertices->operator[](p2);
      middle_angles(pt1, pt2, 0.5f, param_fn, &new_vertex, convention);
      idx[i] = (uint32_t) vertices->size();
      if (param_fn) {
       compute_vertex(&new_vertex, param_fn);
       add_vertex(vertices, new_vertex);
      }
      else {
        // just builds a sphere from radius
        populate_cartesian(new_vertex.pos, new_vertex, convention);
        add_vertex(vertices, new_vertex);
      }
      middle_points[key] = idx[i];
    }
    add_trg(&new_triangles, trg_T(tr.v[0], idx[0], idx[2]));
    add_trg(&new_triangles, trg_T(tr.v[1], idx[1], idx[0]));
    add_trg(&new_triangles, trg_T(tr.v[2], idx[2], idx[1]));
    add_trg(&new_triangles, trg_T(idx[0], idx[1], idx[2]));
  }
  *triangles = new_triangles;
}

// dump a mesh in an obj file
void
dump_mesh_to_obj
  (string name,
   const vector<vrtx_T>& vertices,
   const vector<trg_T>& triangles)
{
  ofstream of;
  of.open(name);
  for (uint32_t i = 0; i < (uint32_t) vertices.size(); i++) {
    const float *pt = vertices[i].pos;
    of << "v " << pt[0] << " " << pt[1] << " " << pt[2] << endl;
  }
  of << endl;
  for (uint32_t i = 0; i < (uint32_t) triangles.size(); i++) {
    const uint32_t *idx = triangles[i].v;
    of << "f " << (1 + idx[0]) << " " << (1 + idx[1]) << " " << (1 + idx[2]) << endl;
  }
  of.close();
}

void time_to_string(string * str, const char* fmt) {
  time_t t = time(0);
  struct tm * now = localtime(&t);
  char buffer[128];
  strftime(buffer, sizeof(buffer), fmt, now);
  *str = string(buffer);
}

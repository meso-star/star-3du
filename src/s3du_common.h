/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#ifndef S3DU_COMMON_H
#define S3DU_COMMON_H

#include "s3du.h"

#include <vector>
#include <limits>
#include <algorithm>
#include <string>
#include <map>

using std::vector;
using std::max;
using std::min;
using std::numeric_limits;
using std::map;
using std::string;

/******************************************************************************
* basic types for vertices and triangles
******************************************************************************/

class vrtx_T {
public:
  vrtx_T() : theta(0), phi(0), r(0) {
    pos[0] =  pos[1] = pos[2] = 0;
  }

  vrtx_T(const float p[3]) : theta(0), phi(0), r(0) {
    pos[0] = p[0]; pos[1] = p[1]; pos[2] = p[2];
  }

public:
  float pos[3];
  float theta, phi, r;
};

class trg_T {
public:
  trg_T(uint32_t v0, uint32_t v1, uint32_t v2) {
    v[0] = v0; v[1] = v1; v[2] = v2;
    locked = false;
  }

  trg_T(const uint32_t v[3]) : trg_T(v[0], v[1], v[2]) {
  }

public:
  uint32_t v[3];
  bool locked;
};

/******************************************************************************
* type for keys to refer the edge between points p1 and p2
******************************************************************************/
typedef uint64_t key_T;

inline key_T
make_key(uint32_t p1, uint32_t p2) {
  ASSERT(p1 != numeric_limits<uint32_t>::max());
  ASSERT(p2 != numeric_limits<uint32_t>::max());
  return (((uint64_t) min(p1, p2)) << 32) | max(p1, p2);
}

inline uint32_t
first(key_T k) { return (uint32_t)(k >> 32); }

inline uint32_t
second(key_T k) { return uint32_t(k); }

/******************************************************************************
* type to store information for segments refered to by keys
******************************************************************************/
struct edge_info_T{ 
  edge_info_T()
    : m_idx(numeric_limits<uint32_t>::max()), w(0), crest_w(0), 
    crest_conflict_at_level(numeric_limits<uint32_t>::max()),
    selectable(3)
  {}
  edge_info_T(
    uint32_t idx, 
    float weight, 
    float crest_weight
    )
    : m_idx(idx), w(weight), crest_w(crest_weight),
    crest_conflict_at_level(numeric_limits<uint32_t>::max()),
    selectable(3)
  {}
  uint32_t m_idx;
  float w, crest_w;
  uint32_t crest_conflict_at_level;
  unsigned char selectable;
};
typedef map<key_T, edge_info_T> middle_pt_idx_map_T;

/******************************************************************************
* helper macros
******************************************************************************/
#define GEO_NB_RINGS(level) (6 + (level))
#define GEO_NB_SLICES(level) (8 + 2 * (level))

/******************************************************************************
* helper type for user data
******************************************************************************/
typedef struct { vector<vrtx_T>* vrtcs; vector<trg_T>* trgs; } user_data_T;

/******************************************************************************
* functions
******************************************************************************/

void
_get_ids(const unsigned itri, unsigned ids[3], void* data);

void
_get_position(const unsigned ivert, float position[3], void* data);

void
populate_cartesian(float *dest, const vrtx_T& pt, float r, enum parametrization_T convention);

void
populate_cartesian(float *dest, const vrtx_T& pt, enum parametrization_T convention);

void
add_vertex(std::vector<vrtx_T>* v, const vrtx_T& pt);

void
compute_vertex(vrtx_T* pt, param_fn_T param_fn);

void
add_trg(std::vector<trg_T> * triangles, const trg_T& t);

void
check_convention(const vrtx_T& pt, enum parametrization_T convention);

void
check_convention(float theta, float phi, enum parametrization_T convention);

float
clamp_theta(float theta, enum parametrization_T convention);

float
clamp_phi(float phi, enum parametrization_T convention);

float
middle_angles
  (const vrtx_T& pt1, 
   const vrtx_T& pt2,
   float fraction,
   param_fn_T param_fn,
   vrtx_T* middle,
   parametrization_T convention);

void
create_icosphere_0
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float radius,
   enum mesh_orientation_T orientation,
   enum parametrization_T convention);

void
create_geodesic_sphere
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float radius,
   uint32_t level,
   enum mesh_orientation_T orientation,
   enum parametrization_T convention);

void
one_step_diadic_refine
  (vector<vrtx_T>* vertices, 
   vector<trg_T>* triangles,
   parametrization_T convention,
   param_fn_T param_fn, 
   float radius = 0);

void
dump_mesh_to_obj
  (string name,
   const vector<vrtx_T>& vertices,
   const vector<trg_T>& triangles);

void
time_to_string(string * str, const char* fmt);

#endif /* S3DU_COMMON_H */


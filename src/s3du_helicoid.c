/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use,
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited
* liability.
*
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security.
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"
#include "s3du_common.h"

#include <star/s3d.h>

#include<vector>
#include<string>
#include <fstream>
#include<iostream>
#include<algorithm>
#include <stdexcept>


#include<stdlib.h>
#include<stdio.h>
#include<stdint.h>
#define _USE_MATH_DEFINES
#include <math.h>

using std::vector;
using std::string;
using std::ofstream;
using std::min;
using std::max;
using std::cout;
using std::endl;
using std::cos;
using std::sin;
using std::atan2;
using std::sqrt;
using std::exception;

/******************************************************************************
* default options
******************************************************************************/

const struct helicoidal_mesh_options_T default_helicoidal_mesh_options = {
  { default_center[0], default_center[1], default_center[2] },
  default_level,
  default_mesh_orientation,
  default_rotation,
  default_termination,
  default_starting_angle,
  default_output_final_mesh,
  default_output_files_base_name
};

/******************************************************************************
* extended type for options
******************************************************************************/

struct helico_options_T : public helicoidal_mesh_options_T {
  string base_name;
};

/******************************************************************************
* functions
******************************************************************************/

static void
make_helicoidal_mesh
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float H,
   float P,
   float D,
   float d,
   const struct helico_options_T& options)
{
  ASSERT(H > 0);
  ASSERT(P > 0);
  ASSERT(D > 0);
  ASSERT(D > d);
  ASSERT(d > 0);

  const float theta_range = 2 * (float) M_PI * H / P;
  const float theta_end = options.starting_angle + theta_range;
  const int ntheta = 1 + max(1, (int) ((4 + (float) options.level * 2) * theta_range / M_PI));
  const float theta_step = theta_range / (float) (ntheta - 1);
  const float dextro = (options.rotation == DEXTRO) ? +1.0f : -1.0f;

  const float a = (D - d) / 2;
  const float b = d / 2;
  const float h = P / (float) (2 * M_PI);
  const float _1_sqrta2h2 = 1 / sqrt(a * a + h * h);
  const float slope = atan2(h, a);
  const float cslope = cos(slope);
  const float sslope = sin(slope);

  // CW or CCW?
  const int i1 =
    ((options.mesh_orientation == CCW) == (options.rotation == DEXTRO)) ? 1 : 2;
  const int i2 = 3 - i1;
  
  // number of vertices per ring
  const uint32_t rsize = (1 + options.level) * 6;
  // total number of vertices
  unsigned int nbv = ntheta * rsize;
  // total number of triangles
  unsigned int nbt = 2 * (ntheta - 1) * rsize;
  if (options.termination != NONE) {
    for (uint32_t l = 0; l <= options.level; l++) {
      nbv += l ? l * 12 : 2;
      nbt += (2 * l + 1) * 12;
    }
  }
  vertices->reserve(nbv);
  triangles->reserve(nbt);

  //
  // create the tube
  //
  for (int t = 0; t < ntheta; t++) {
    const float dtheta = (float) t * theta_step;
    const float stheta = sin(options.starting_angle + dtheta);
    const float ctheta = cos(options.starting_angle + dtheta);
    const uint32_t vsize = (uint32_t) vertices->size();
    ASSERT(vsize == t * rsize);

    // create a new ring of vertices
    const float dphi = (float) (2 * M_PI) / (float) rsize;
    for (uint32_t p = 0; p < rsize; p++) {
      const float phi = (float) p * dphi;
      const float sphi = sin(phi);
      const float cphi = cos(phi);

      vrtx_T v;
      v.pos[0] = (a + b * cphi) * ctheta + b * h * _1_sqrta2h2 * stheta * sphi;
      v.pos[1] = dextro *((a + b * cphi) * stheta
        - b * h * _1_sqrta2h2 * ctheta * sphi);
      v.pos[2] = - H / 2 + h * dtheta + b * a * _1_sqrta2h2 * sphi;

      add_vertex(vertices, v);
    }

    // connect the current ring to the previous one
    if (t > 0) {
      for (uint32_t v = 0; v < rsize; v++) {
        uint32_t idx[3];
        const uint32_t idx1 = v;
        const uint32_t idx2 = (v + 1) % rsize;
        idx[0] = vsize + idx1;
        idx[i1] = vsize + idx2;
        idx[i2] = vsize - rsize + idx1;
        add_trg(triangles, trg_T(idx));
        idx[0] = vsize - rsize + idx1;
        idx[i1] = vsize + idx2;
        idx[i2] = vsize - rsize + idx2;
        add_trg(triangles, trg_T(idx));
      }
    }

    ASSERT(vertices->size() == (t + 1) * rsize);
    ASSERT(triangles->size() == 2 * t * rsize);
  }

  if (options.termination != NONE) {
    const uint32_t tube_last_ring_idx = (uint32_t) vertices->size() - rsize;
    //
    // create beginning
    //
    uint32_t last_ring_idx = 0;
    for (int l = (int) options.level; l >= 0; l--) {
      const uint32_t outer_fst_idx = last_ring_idx;
      const uint32_t inner_fst_idx = (uint32_t) vertices->size();
      const int current_rsize = l ? l * 6 : 1;
      const int external_rsize = (l + 1) * 6;
      const float dphi = (float) (2 * M_PI) / (float) current_rsize;
      const float theta = options.starting_angle;
      const float stheta = sin(theta);
      const float ctheta = cos(theta);
      float term[3];
      float current_b = 0;
      switch (options.termination) {
      case SPHERE: {
        current_b = b * sin(0.5f * (float) M_PI * (float) l / (float) (options.level + 1));
        const float z_tg = sqrt(b * b - current_b * current_b);
        term[0] = z_tg * cslope * stheta;
        term[1] = -z_tg * cslope * ctheta;
        term[2] = -z_tg * sslope;
        break;
      }
      case DISK: {
        current_b = b * (float) l / (float) (options.level + 1);
        term[0] = term[1] = term[2] = 0;
        break;
      }
      default: FATAL("Unreachable code\n"); break;
      }
      for (int p = 0; p < current_rsize; p++) {
        const float phi = (float) p * dphi;
        const float sphi = sin(phi);
        const float cphi = cos(phi);
        vrtx_T v;

        v.pos[0] = (a + current_b * cphi) * ctheta
          + (current_b * h * _1_sqrta2h2) * stheta * sphi + term[0];
        v.pos[1] = dextro * ((a + current_b * cphi) * stheta
          - (current_b * h * _1_sqrta2h2) * ctheta * sphi + term[1]);
        v.pos[2] = -H / 2 + current_b * a * _1_sqrta2h2 * sphi + term[2];

        add_vertex(vertices, v);
      }
      uint32_t inner_idx = inner_fst_idx;
      for (int p = 0; p < external_rsize; p++) {
        uint32_t outer_idx = outer_fst_idx + p;
        uint32_t outer_next_idx = (p == external_rsize - 1)
          ? outer_fst_idx : outer_idx + 1;
        uint32_t inner_next_idx = (inner_idx == inner_fst_idx + current_rsize - 1)
          ? inner_fst_idx : inner_idx + 1;
        // connect the current ring to the previous one
        // use vertices' idx from vertices (not from ring)
        uint32_t idx[3];
        idx[0] = outer_idx; 
        idx[i1] = outer_next_idx; 
        idx[i2] = inner_idx;
        add_trg(triangles, trg_T(idx));
        // every 2*level+1 triangles, skip this one
        // the last ring is the center and this should be always skipped 
        if ((p + 1) % (l + 1)) {
          idx[0] = outer_next_idx; 
          idx[i1] = inner_next_idx; 
          idx[i2] = inner_idx;
          add_trg(triangles, trg_T(idx));
          inner_idx = inner_next_idx;
        }
      }
      last_ring_idx = inner_fst_idx;
    }

    //
    // create ending
    //
    last_ring_idx = tube_last_ring_idx;
    for (int l = (int) options.level; l >= 0; l--) {
      const uint32_t outer_fst_idx = last_ring_idx;
      const uint32_t inner_fst_idx = (uint32_t) vertices->size();
      const int current_rsize = l ? l * 6 : 1;
      const int external_rsize = (l + 1) * 6;
      const float dphi = (float) (2 * M_PI) / (float) current_rsize;
      const float theta = theta_end;
      const float stheta = sin(theta);
      const float ctheta = cos(theta);
      float current_b = 0;
      float term[3];
      switch (options.termination) {
      case SPHERE: {
        current_b = b * sin(0.5f * (float) M_PI * (float) l / (float) (options.level + 1));
        const float z_tg = sqrt(b * b - current_b * current_b);
        term[0] = -z_tg * cslope * stheta;
        term[1] = z_tg * cslope * ctheta;
        term[2] = z_tg * sslope;
        break;
      }
      case DISK: {
        current_b = b * (float) l / (float) (options.level + 1);
        term[0] = term[1] = term[2] = 0;
        break;
      }
      default: FATAL("Unreachable code\n"); break;
      }

      for (int p = 0; p < current_rsize; p++) {
        const float phi = (float) p * dphi;
        const float sphi = sin(phi);
        const float cphi = cos(phi);
        vrtx_T v;

        v.pos[0] = (a + current_b * cphi) * ctheta
          + (current_b * h * _1_sqrta2h2) * stheta * sphi + term[0];
        v.pos[1] = dextro * ((a + current_b * cphi) * stheta
          - (current_b * h * _1_sqrta2h2) * ctheta * sphi + term[1]);
        v.pos[2] = H / 2 + current_b * a * _1_sqrta2h2 * sphi + term[2];

        add_vertex(vertices, v);
      }
      uint32_t inner_idx = inner_fst_idx;
      for (int p = 0; p < external_rsize; p++) {
        uint32_t outer_idx = outer_fst_idx + p;
        uint32_t outer_next_idx = (p == external_rsize - 1)
          ? outer_fst_idx : outer_idx + 1;
        uint32_t inner_next_idx = (inner_idx == inner_fst_idx + current_rsize - 1)
          ? inner_fst_idx : inner_idx + 1;
        // connect the current ring to the previous one
        // use vertices' idx from vertices (not from ring)
        uint32_t idx[3];
        idx[0] = outer_idx; 
        idx[i1] = inner_idx; 
        idx[i2] = outer_next_idx;
        add_trg(triangles, trg_T(idx));
        // every 2*level+1 triangles, skip this one
        // the last ring is the center and this should be always skipped 
        if ((p + 1) % (l + 1)) {
          idx[0] = outer_next_idx; 
          idx[i1] = inner_idx; 
          idx[i2] = inner_next_idx;
          add_trg(triangles, trg_T(idx));
          inner_idx = inner_next_idx;
        }
      }
      last_ring_idx = inner_fst_idx;
    }
  }
  ASSERT(vertices->size() == nbv);
  ASSERT(triangles->size() == nbt);

  // the mesh is ready, but centered at <0,0,0>: offset vertices
  for (size_t i = 0; i < vertices->size(); i++) {
    float *pt = vertices->operator[](i).pos;
    for (int j = 0; j < 3; j++) {
      pt[j] += options.center[j];
    }
  }

  // dump the final mesh
  if (options.output_final_mesh) {
    dump_mesh_to_obj
      (options.base_name + "helicoid.obj", *vertices, *triangles);
  }
}

/*******************************************************************************
 * Exported s3du_shape functions:
 ******************************************************************************/
res_T
s3du_shape_create_helicoid
(struct s3d_device* dev,
   struct s3d_shape** out_pp,
   float H,
   float P,
   float D,
   float d,
   const struct helicoidal_mesh_options_T* _options)
{
  struct s3d_shape* pp = NULL;
  struct s3d_vertex_data attrib;
  vector<vrtx_T> vertices;
  vector<trg_T> triangles;
  user_data_T udata;
  helico_options_T options;
  res_T res = RES_OK;

  if (!dev || !out_pp) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = s3d_shape_create_mesh(dev, &pp);
  if (res != RES_OK) {
    goto error;
  }

  options.helicoidal_mesh_options_T::operator=(*_options);
  if (options.output_final_mesh) {
    time_to_string(&options.base_name, options.output_files_base_name);
  }

  try {
    make_helicoidal_mesh(&vertices, &triangles, H, P, D, d, options);
  }
  catch (std::bad_alloc) {
    res = RES_MEM_ERR;
  }
  catch (...) {
    ASSERT(false); // not supposed to occur!
    res = RES_BAD_OP;
  }

  attrib.type = S3D_FLOAT3;
  attrib.usage = S3D_POSITION;
  attrib.get = _get_position;

  udata.vrtcs = &vertices;
  udata.trgs = &triangles;
  res = s3d_mesh_setup_indexed_vertices(pp, (unsigned) triangles.size(),
    _get_ids, (unsigned) vertices.size(), &attrib, 1, &udata);

  if (res != RES_OK) {
    goto error;
  }

exit:
  if (out_pp)
    *out_pp = pp;
  return res;

error:
  if (pp) {
    S3D(shape_ref_put(pp));
    pp = NULL;
  }
  goto exit;
}

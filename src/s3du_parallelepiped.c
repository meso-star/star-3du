/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use,
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited
* liability.
*
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security.
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms. */

#define PP_DUMP_FINAL_MESH 0

#include "s3du.h"
#include "s3du_common.h"

#include <star/s3d.h>
#include <rsys/float3.h>

/******************************************************************************
* default options
******************************************************************************/

const struct meshed_parallelepiped_options_T default_meshed_parallelepiped_options = {
  { default_center[0], default_center[1], default_center[2] },
  default_mesh_orientation,
  default_output_final_mesh,
  default_output_files_base_name
};

/******************************************************************************
* extended type for options
******************************************************************************/

struct pped_options_T : public meshed_parallelepiped_options_T {
  string base_name;
  bool direct;
};

static const unsigned pped_indices [] =
{ /* outwards normals */
  0, 3, 2, 2, 1, 0, /* Bottom */
  4, 5, 6, 6, 7, 4, /* Top */
  0, 4, 7, 7, 3, 0, /* Left */
  6, 5, 1, 1, 2, 6, /* Right */
  4, 0, 1, 1, 5, 4, /* Back */
  7, 6, 2, 2, 3, 7, /* Front */
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
make_pped_mesh
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   const float edge1[3],
   const float edge2[3],
   const float edge3[3],
   const pped_options_T& options)
{
  float tmp1[3], tmp2[3], tmp3[3];
  vertices->reserve(8);
  triangles->reserve(12);
  // first level's vertices
  f3_mulf(tmp1, edge1, -0.5f);
  f3_mulf(tmp2, edge2, -0.5f);
  f3_mulf(tmp3, edge3, -0.5f);
  f3_add(vertices->operator[](0).pos, options.center, f3_add(tmp1, tmp3, f3_add(tmp1, tmp1, tmp2)));
  f3_add(vertices->operator[](1).pos, vertices->operator[](0).pos, edge2);
  f3_add(vertices->operator[](2).pos, vertices->operator[](1).pos, edge1);
  f3_add(vertices->operator[](3).pos, vertices->operator[](0).pos, edge1);
  // second level's vertices
  for (int i = 0; i < 4; i++) {
    f3_add(vertices->operator[](i + 4).pos, vertices->operator[](i).pos, edge3);
  }
  //triangles
  for (int i = 0; i < 12; i++) {
    // CW or CCW?
    int i0 = 3 * i;
    int i1 = i0 + (options.direct == (options.mesh_orientation == CW) ? 1 : 2);
    int i2 = 6 * i + 3 - i1;
    triangles->push_back(trg_T(pped_indices[i0], pped_indices[i1], pped_indices[i2]));
  }

  if (options.output_final_mesh) {
    dump_mesh_to_obj
      (options.base_name + "parallelepiped.obj", *vertices, *triangles);
  }
}

static void
pped_get_ids(const unsigned itri, unsigned ids[3], void* data)
{
  const unsigned id = itri * 3;
  ASSERT(id + 2 < sizeof(pped_indices) / sizeof(*pped_indices));
  (void)data;
  ids[0] = pped_indices[id + 0];
  ids[1] = pped_indices[id + 1];
  ids[2] = pped_indices[id + 2];
}

static void
pped_get_position(const unsigned ivert, float position[3], void* data)
{
  const unsigned id = ivert * 3;
  const float* pped_vertices = (float *)data;
  position[0] = pped_vertices[id + 0];
  position[1] = pped_vertices[id + 1];
  position[2] = pped_vertices[id + 2];
}

/*******************************************************************************
 * Exported s3du_shape functions:
 ******************************************************************************/
res_T
s3du_shape_create_meshed_parallelepiped
  (struct s3d_device* dev,
   struct s3d_shape** out_pp,
   const float edge1[3],
   const float edge2[3],
   const float edge3[3],
   const struct meshed_parallelepiped_options_T* _options)
{
  struct s3d_shape* pp = NULL;
  vector<vrtx_T> vertices(8);
  vector<trg_T> triangles;
  struct s3d_vertex_data attrib;
  pped_options_T options;
  res_T res = RES_OK;
  float n[3];
  float direct = f3_dot(edge3, f3_cross(n, edge1, edge2));

  if (!dev || !out_pp) {
    res = RES_BAD_ARG;
    goto error;
  }

  // check that edge1, edge2, edge3 is a base of R3
  if (fabs(f3_dot(edge1, edge2)) < 1e-6 || fabs(direct) < 1e-6) {
    // edge1 = x.edge2 or edge3 = x.edge1 + y.edge2
    res = RES_BAD_ARG;
    goto error;
  }

  res = s3d_shape_create_mesh(dev, &pp);
  if (res != RES_OK) {
    goto error;
  }

  options.meshed_parallelepiped_options_T::operator=(*_options);
  options.direct = direct > 0;
  if (options.output_final_mesh) {
    time_to_string(&options.base_name, options.output_files_base_name);
  }

  make_pped_mesh(&vertices, &triangles, edge1, edge2, edge3, options);

  attrib.type = S3D_FLOAT3;
  attrib.usage = S3D_POSITION;
  attrib.get = pped_get_position;

  res = s3d_mesh_setup_indexed_vertices
    (pp, 12, pped_get_ids, 8, &attrib, 1, &vertices);
  if (res != RES_OK) {
    goto error;
  }

exit:
  if (out_pp)
    *out_pp = pp;
  return res;

error:
  if (pp) {
    S3D(shape_ref_put(pp));
    pp = NULL;
  }
  goto exit;
}

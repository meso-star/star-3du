/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use,
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited
* liability.
*
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security.
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"
#include "s3du_common.h"

#include <star/s3d.h>
#include <rsys/rsys.h>
#include <rsys/float3.h>

#include <vector>
#include <map>
#include <set>
#include <limits>
#include <random>
#include <fstream>
#include <iostream>
#include <string>
#define _USE_MATH_DEFINES
#include <math.h>

using std::max;
using std::min;
using std::vector;
using std::pair;
using std::map;
using std::set;
using std::make_pair;
using std::numeric_limits;
using std::string;
using std::ofstream;
using std::cout;
using std::endl;
using std::to_string;
using std::log;
using std::sqrt;
using std::cos;
using std::acos;
using std::atan;
using std::fmod;
using std::fabs;
using std::pow;

//#define TRACES_2_COUT _DEBUG
#define TRACES_2_COUT 0

/******************************************************************************
* default options
******************************************************************************/

const struct param_mesh_options_T default_mesh_param_function_options = {
  { default_center[0], default_center[1], default_center[2] },
  default_level,
  default_refinement_policy,
  default_parametrization,
  default_mesh_orientation,
  default_sphere_type,
  default_relative_error,
  default_relative_min_edge,
  default_relative_min_surf,
  default_initial_samples_per_radian,
  default_final_samples_per_radian,
  default_output_final_mesh,
  default_output_refinement_steps,
  default_output_files_base_name
};

/******************************************************************************
* extended type for options
******************************************************************************/

struct param_options_T : public param_mesh_options_T {
  // utilities
  float scaled_max_offset;
  float scaled_min_edge;
  float scaled_min_4surf2;
  float crest_line_threshold;
  float max_radius;
  string base_name;
};

/******************************************************************************
* sampled points and their weights; used only for debug output
******************************************************************************/
typedef struct{ vrtx_T v; float w; } weighted_vrtx_T;
typedef vector<weighted_vrtx_T> sampling_path_T;

/******************************************************************************
 * Helper functions
 ******************************************************************************/

// rate a point in function of its distance to the original arc and segment
// this rating must be homogen to a distance to be used by the
// user defined predicate that decide if this distance is acceptable or not
static float
rate_vertex(const vrtx_T& pt, float segment_radius) {
  float dr = pt.r - segment_radius;
  return sqrt(dr * dr);
}

// rate crest lines intensity
static const float MAX_CREST_LINE_SCORE = (float) M_PI;

static float
rate_crest_line(float dr1, float dr2) {
  ASSERT(dr1 * dr2 <= 0);
  float n0 = sqrt(1 + 128 * dr1 * dr1);
  float n2 = sqrt(1 + 128 * dr2 * dr2);
  float cos_a = (1 + dr1 * dr2) / (n0 * n2);
  float a = acos(max(-1.0f, min(cos_a, 1.0f)));
  ASSERT(0 <= a && a <= MAX_CREST_LINE_SCORE);
  return a;
}

// return the relative position of the final point relative to
// the initial [left right] segment in ]0 1[
static float
locate_crest_line
  (vrtx_T left,
   vrtx_T center,
   vrtx_T right,
   vrtx_T* crest,
   float angle,
   param_fn_T param_fn,
   const param_options_T& options,
   sampling_path_T* path)
{
  ASSERT(angle > 0);
  float samples_per_radian = 1 / angle;
  float fraction = 0;
  float ratio = 0.25f;
  vrtx_T tmp1, tmp2;
  vrtx_T* pt[5] = { &left, &tmp1, &center, &tmp2, &right };
  int hs_idx = -1;
  do {
    float slope[4];
    middle_angles(*pt[0], *pt[2], 0.5f, param_fn, &tmp1, options.convention);
    middle_angles(*pt[2], *pt[4], 0.5f, param_fn, &tmp2, options.convention);

    float hs = -numeric_limits<float>::max();
    for (int i = 0; i < 4; i++) {
      slope[i] = (pt[i + 1]->r - pt[i]->r) * samples_per_radian;
      if (i == 0) continue;
      if (slope[i - 1] * slope[i] <= 0) {
        float s = rate_crest_line(slope[i - 1], slope[i]);
        ASSERT(0 <= s && s <= MAX_CREST_LINE_SCORE);
        if (s > hs) {
          hs = s;
          hs_idx = i;
        }
      }
    }
    ASSERT(0 < hs_idx && hs_idx < 4);
    if (hs_idx < 0) {
      // cannot confirm the crest presence
      // could be possible if the angle is too small
      break;
    }

    if (0 != hs_idx -1) *pt[0] = *pt[hs_idx - 1];
    if (4 != hs_idx + 1) *pt[4] = *pt[hs_idx + 1];
    if (2 != hs_idx) *pt[2] = *pt[hs_idx];

    if (path) {
      if (slope[0] * slope[1] <= 0) {
        path->push_back({ tmp1, rate_crest_line(slope[0], slope[1]) });
      }
      if (slope[2] * slope[3] <= 0) {
        path->push_back({ tmp2, rate_crest_line(slope[2], slope[3]) });
      }
    }

    fraction += ratio * (float) (hs_idx - 1);
    samples_per_radian *= 2;
    ratio *= 0.5f;
  } while (samples_per_radian < options.final_samples_per_radian);
  fraction += ratio * (float) hs_idx;
  ASSERT(0 < fraction && fraction < 1);
  *crest = *pt[hs_idx];
  crest->r = param_fn(crest->theta, crest->phi, crest->pos);
  return fraction;
}

// dichotomically locate the better split location for segment [pt1 pt2]
// in the subsegment [left center right]
// return the associated weight and output the corresponding point in *center
static float
locate_best_score
  (const vrtx_T& pt1,
   const vrtx_T& pt2,
   float best_score,
   float fraction,
   float ratio,
   vrtx_T left,
   vrtx_T* center,
   vrtx_T right,
   float angle,
   param_fn_T param_fn,
   const param_options_T& options,
   sampling_path_T* path)
{
  float samples_per_radian = 1 / angle;
  ASSERT(center->r > 0);
  do {
    vrtx_T tmp0, tmp2;
    float sr0, sr2;
    // compute segment radius
    ratio *= 0.5f;
    sr0 = middle_angles(pt1, pt2, fraction - ratio, param_fn, &tmp0, options.convention);
    sr2 = middle_angles(pt1, pt2, fraction + ratio, param_fn, &tmp2, options.convention);
    // rate these points
    float d0 = rate_vertex(tmp0, sr0);
    float d2 = rate_vertex(tmp2, sr2);

    if (path) {
      path->push_back({ tmp0, d0 });
      path->push_back({ tmp2, d2 });
    }

    if (max(d0, d2) > best_score) {
      if (d0 > d2) {
        best_score = d0;
        // new range is [left center]
        right = *center;
        *center = tmp0;
        ASSERT(center->r > 0);
      }
      else {
        best_score = d2;
        // new range is [center right]
        left = *center;
        *center = tmp2;
        ASSERT(center->r > 0);
      }
    }
    else {
      // new range is [tmp0 tmp2]
      left = tmp0;
      right = tmp2;
    }
    samples_per_radian *= 2;
  } while (samples_per_radian < options.final_samples_per_radian);
  ASSERT(center->r > 0);
  return best_score;
}

// locate the better location for a point of the starting mesh
// try to find a crest line on the [pt1 pt2] segment
// if a crest line was found return its score
// and output the point in *result if provided
// otherwise return -FLT_MAX
static float
locate_starting_point
  (const vrtx_T& pt1,
   const vrtx_T& pt2,
   param_fn_T param_fn,
   const param_options_T& options,
   vrtx_T* result,
   sampling_path_T* path = nullptr)
{
  // compute number of samples
  float v1[3] = { 0, 0, 0 }, v2[3] = { 0, 0, 0 };
  f3_normalize(v1, pt1.pos);
  f3_normalize(v2, pt2.pos);
  float c = f3_dot(v1, v2);
  c = max(-1.0f, min(1.0f, c));
  float angle = acos(c);
  //float is = options.initial_samples;
  float is = (float) (int) options.initial_samples_per_radian; /* performance */
  int nb_sample = (int) (angle * is);
  if (nb_sample <= 0) {
    return -numeric_limits<float>::max();
  }

  // sampling
  float max_crest_line_score = -numeric_limits<float>::max();
  uint32_t max_crest_line_idx = numeric_limits<uint32_t>::max();
  float samples_per_radian = (float) nb_sample / angle;
  vector<vrtx_T> sample(nb_sample + 1);
  vector<float> slope(nb_sample + 1);
  
  for (int i = 0; i <= nb_sample; i++) {
    if (i == nb_sample) {
      sample[i].theta = pt2.theta;
      sample[i].phi = pt2.phi;
      sample[i].r = pt2.r;
    }
    else {
      float fraction = (float) (1 + i) / (float) (1 + nb_sample);
      middle_angles(pt1, pt2, fraction, param_fn, &sample[i], options.convention);
    }
    slope[i] = (sample[i].r - (i ? sample[i - 1].r : pt1.r)) * samples_per_radian;
  }
  for (int i = 1; i <= nb_sample; i++) {
    if (slope[i - 1] * slope[i] <= 0) {
      float crest_line_score = rate_crest_line(slope[i - 1], slope[i]);
      if (crest_line_score > max_crest_line_score) {
        max_crest_line_score = crest_line_score;
        max_crest_line_idx = i;
      }
      if (path) {
        path->push_back({ sample[i], crest_line_score });
      }
    }
    else {
      if (path) {
        path->push_back({ sample[i], 0 });
      }
    }
  }
  ASSERT(max_crest_line_score < 0 ||
    (1 <= max_crest_line_idx && (int) max_crest_line_idx <= nb_sample));
  ASSERT(max_crest_line_idx == numeric_limits<uint32_t>::max() ||
    (0 <= max_crest_line_score && max_crest_line_score <= MAX_CREST_LINE_SCORE));

  // if a crest line has been located
  // we will try to locate the precise point recursively here
  float sample_angle = angle / (float) nb_sample;
  if (max_crest_line_score > options.crest_line_threshold) {
    const vrtx_T& left
      = (max_crest_line_idx <= 1) ? pt1 : sample[max_crest_line_idx - 2];
    const vrtx_T& right
      = ((int) max_crest_line_idx == nb_sample) ? pt2 : sample[max_crest_line_idx];
    locate_crest_line(left, sample[max_crest_line_idx - 1], right, result,
      sample_angle, param_fn, options, path);

    return max_crest_line_score;
  }

  return -numeric_limits<float>::max();
}

bool is_pole(const vrtx_T& pt, parametrization_T convention) {
  check_convention(pt, convention);
  switch (convention) {
  case SPHERICAL_1: { // theta in [0 pi] and phi in [0 2pi]
    return fabs(pt.theta - (float) (0.5 * M_PI)) >= (float) (0.49 * M_PI);
  }
  case SPHERICAL_2: { // theta in [-pi/2 +pi/2] and phi in [-pi +pi]
    return fabs(pt.theta) >= (float) (0.49 * M_PI);
  }
  default: {
    ASSERT(false);
    return false;
  }
  }
}

// sample the [pt1 pt2] segment to find the best location to split it
// if a subdivision is decided the weights at the split location is returned
// otherwise result isn't modified and (-FLT_MAX, -FLT_MAX) is returned
// we don't want to split too close to the extremities (min edge)
static pair<float, float>
find_best_split_point
  (const vrtx_T& pt1,
   const vrtx_T& pt2,
   param_fn_T param_fn,
   const param_options_T& options,
   float edge_len,
   vrtx_T* result,
   sampling_path_T* path = nullptr)
{
  // compute the number of samples
  float v1[3] = { 0, 0, 0 }, v2[3] = { 0, 0, 0 };
  f3_normalize(v1, pt1.pos);
  f3_normalize(v2, pt2.pos);
  float c = f3_dot(v1, v2);
  c = max(-1.0f, min(1.0f, c));
  float angle = acos(c);
  float is = (float) (int) options.initial_samples_per_radian; /* performance */
  int nb_sample = (int) (angle * is);
  // compute the range of samples that won't create edges shorter than scaled_min_len
  float margin = options.scaled_min_edge / edge_len;
  ASSERT(0 < margin && margin < 1);
  int first_sample = (int) ((float) (nb_sample + 1) * margin);
  ASSERT(0 <= first_sample);
  int last_sample = (int) ((float) (nb_sample + 1) * (1 - margin));
  ASSERT(last_sample <= nb_sample);


  //int last_sample = (int) ((float) nb_sample * (1 - margin));
  //ASSERT(last_sample == 0 || last_sample < nb_sample);

  if (first_sample >= last_sample) {
    return make_pair(-numeric_limits<float>::max(), -numeric_limits<float>::max());
  }

  float samples_per_radian = (float) nb_sample / angle;
  float best_score = -numeric_limits<float>::max();
  int best_score_idx = numeric_limits<int>::max();
  float best_crest_score = -numeric_limits<float>::max();
  int best_crest_idx = numeric_limits<int>::max();
  vector<vrtx_T> sample(nb_sample + 1);
  vector<float> slope(nb_sample + 1);

  for (int i = max(0, first_sample - 2); i <= last_sample; i++) {
    // compute sample
    float fraction = (float) (1 + i) / (float) (1 + nb_sample);
    float segment_radius;
    if (i == nb_sample) {
      sample[i].theta = pt2.theta;
      sample[i].phi = pt2.phi;
      sample[i].r = pt2.r;
      segment_radius = pt2.r;
    }
    else {
      segment_radius = middle_angles(pt1, pt2, fraction, param_fn, &sample[i], options.convention);
    }
    ASSERT(sample[i].r > 0);
    if (i < first_sample) {
      continue;
    }
    // rate sample
    float score = rate_vertex(sample[i], segment_radius);
    ASSERT(score >= 0);
    if (score > best_score) {
      best_score = score;
      best_score_idx = i;
    }
    if (path) {
      path->push_back({ sample[i], score });
    }
  }
  ASSERT(first_sample <= best_score_idx && best_score_idx <= last_sample);

  for (int i = max(1, first_sample - 1); i <= last_sample; i++) {
    slope[i] = (sample[i].r - sample[i - 1].r) * samples_per_radian;
    if (i == max(1, first_sample - 1)) {
      continue;
    }
    if (slope[i - 1] * slope[i] <= 0) {
      float crest_line_score = rate_crest_line(slope[i - 1], slope[i]);
      if (crest_line_score > best_crest_score) {
        best_crest_score = crest_line_score;
        best_crest_idx = i;
      }
      if (path) {
        path->push_back({ sample[i], crest_line_score });
      }
    }
    else {
      if (path) {
        path->push_back({ sample[i], 0 });
      }
    }
  }
  ASSERT(best_crest_score < 0 ||
    (first_sample <= best_crest_idx && best_crest_idx <= last_sample));
  ASSERT(best_crest_idx == numeric_limits<int>::max() ||
    (0 <= best_crest_score && best_crest_score <= MAX_CREST_LINE_SCORE));

  // if a crest line has been located
  // we will try to precise the location recursively here
  float sample_angle = angle / (float) nb_sample;
  if (best_crest_score > 0) {
    const vrtx_T& left
      = (best_crest_idx <= 1) ? pt1 : sample[best_crest_idx - 2];
    const vrtx_T& right
      = ((int) best_crest_idx == nb_sample) ? pt2 : sample[best_crest_idx];
    locate_crest_line(left, sample[best_crest_idx - 1], right, result,
      sample_angle, param_fn, options, path);
    if (best_crest_score > options.crest_line_threshold) {
      // no need to compute the other score as the crest property takes priority
      ASSERT(result->r > 0);
      ASSERT(best_crest_score >= 0);
      return make_pair(-numeric_limits<float>::max(), best_crest_score);
    }
  }

  // we want to locate the best split location based on weights
  const vrtx_T& left = (best_score_idx == 0) ? pt1 : sample[best_score_idx - 1];
  vrtx_T center = sample[best_score_idx];
  const vrtx_T& right
    = ((int) best_score_idx == nb_sample - 1) ? pt2 : sample[best_score_idx + 1];
  // fraction = ||pt1 center|| / ||pt1 pt2||
  float fraction = (float) (1 + (int) best_score_idx) / (float) (1 + nb_sample);
  // ratio =  ||left center|| / ||pt1 pt2||
  float ratio = 1 / (float) (1 + nb_sample);
  best_score
    = locate_best_score(pt1, pt2, best_score, fraction, ratio, left, &center,
    right, sample_angle, param_fn, options, path);
  *result = center;

  ASSERT(result->r > 0);
  ASSERT(best_score >= 0);
  return make_pair(best_score, best_crest_score);
}

// compute the 'best' point to split a segment
// insert data into middle_point_candidates
// and return an iterator to the middle point 
static middle_pt_idx_map_T::iterator
get_middle_point
  (param_fn_T param_fn,
   const param_options_T& options,
   vector<vrtx_T>* vertices,
   middle_pt_idx_map_T* middle_point_candidates,
   const key_T& key,
   sampling_path_T* path = nullptr)
{
  auto ref = middle_point_candidates->find(key);
  if (ref != middle_point_candidates->end()) {
    // already computed
    return ref;
  }

  uint32_t p1 = first(key);
  uint32_t p2 = second(key);
  const vrtx_T& pt1 = vertices->operator[](p1);
  const vrtx_T& pt2 = vertices->operator[](p2);

  // don't subdivide a too short edge
  float tmp[3];
  f3_sub(tmp, pt1.pos, pt2.pos);
  float edge_len = f3_len(tmp);
  if (edge_len >= 2 * options.scaled_min_edge) {
    vrtx_T middle;
    pair<float, float> score
      = find_best_split_point(pt1, pt2, param_fn, options, edge_len, &middle, path);

    if (max(score.first, score.second) > 0) {
      uint32_t idx = (uint32_t) vertices->size();
      add_vertex(vertices, middle);
      auto e = edge_info_T(idx, score.first, score.second);
      return middle_point_candidates->insert(make_pair(key, e)).first;
    }
  }

  // no subdivision
  auto e = edge_info_T
    (numeric_limits<uint32_t>::max(),
    -numeric_limits<float>::max(),
    -numeric_limits<float>::max());
  auto p = make_pair(key, e);
  return middle_point_candidates->insert(p).first;
}

void 
dump_current_mesh_to_vtk
  (const param_options_T& options,
   unsigned stage, 
   const vector<vrtx_T>& vertices, 
   const vector<trg_T>& triangles)
{
  ofstream of;
  of.open(options.base_name + "stage_" + to_string(stage) + "_starting_mesh.vtk",
    ofstream::out);
  of << "# vtk DataFile Version 3.0" << endl;
  of << "Starting mesh for stage " << stage << endl;
  of << "ASCII" << endl;
  of << "DATASET UNSTRUCTURED_GRID" << endl;
  of << "POINTS " << vertices.size() << " float" << endl;
  for (uint32_t i = 0; i < (uint32_t) vertices.size(); i++) {
    const float *pt = vertices[i].pos;
    of << (pt[0] + options.center[0])
      << " " << (pt[1] + options.center[1])
      << " " << (pt[2] + options.center[2])
      << endl;
  }
  of << "CELLS " << triangles.size() << " " << (4 * triangles.size()) << endl;
  for (uint32_t i = 0; i < (uint32_t) triangles.size(); i++) {
    const uint32_t *idx = triangles[i].v;
    of << "3 " << idx[0] << " " << idx[1] << " " << idx[2] << endl;
  }
  of << "CELL_TYPES " << triangles.size() << endl;
  for (uint32_t i = 0; i < (uint32_t) triangles.size(); i++) {
    of << "5 ";
  }
  of << endl;
  of.close();
}

void 
dump_sampling_to_vtk
  (const param_options_T& options,
   unsigned stage,
   const sampling_path_T& path)
{
  // dump all the weighted points to see the algorithm's path
  ofstream of;
  of.open(options.base_name + "stage_" + to_string(stage) + "_sampling.vtk",
    ofstream::out);
  of << "# vtk DataFile Version 3.0" << endl;
  of << "Sampling for the stage " << stage << endl;
  of << "ASCII" << endl;
  of << "DATASET UNSTRUCTURED_GRID" << endl;
  of << "POINTS " << path.size() << " float" << endl;
  for (uint32_t i = 0; i < (uint32_t) path.size(); i++) {
    const float *pt = path[i].v.pos;
    of << (pt[0] + options.center[0])
      << " " << (pt[1] + options.center[1])
      << " " << (pt[2] + options.center[2])
      << endl;
  }
  of << "CELLS " << path.size() << " " << (2 * path.size()) << endl;
  for (uint32_t i = 0; i < (uint32_t) path.size(); i++) {
    of << "1 " << i << endl;
  }
  of << "CELL_TYPES " << path.size() << endl;
  for (uint32_t i = 0; i < (uint32_t) path.size(); i++) {
    of << "1 ";
  }
  of << endl;
  of << "CELL_DATA " << path.size() << endl;
  of << "SCALARS weights float" << endl;
  of << "LOOKUP_TABLE default" << endl;
  for (uint32_t i = 0; i < (uint32_t) path.size(); i++) {
    of << path[i].w << endl;
  }
  of << endl;
  of.close();
}

void
dump_subdivided_edges_to_vtk
  (const param_options_T& options,
   unsigned stage,
   const middle_pt_idx_map_T& middle_point_candidates,
   const vector<vrtx_T>& vertices,
   const set<key_T>& subdivided)
{
  // dump the edge subdivision points and weights
  // draw the original segment with its weight
  // and the subsegments if subdivided
  ofstream of;
  of.open(options.base_name + "stage_" + to_string(stage) + "_edges.vtk",
    ofstream::out);
  of << "# vtk DataFile Version 3.0" << endl;
  of << "Sampling for the stage " << stage << endl;
  of << "ASCII" << endl;
  of << "DATASET UNSTRUCTURED_GRID" << endl;
  // keep it simple, keep all vertices
  of << "POINTS " << vertices.size() << " float" << endl;
  for (uint32_t i = 0; i < (uint32_t) vertices.size(); i++) {
    const float *pt = vertices[i].pos;
    of << (pt[0] + options.center[0])
      << " " << (pt[1] + options.center[1])
      << " " << (pt[2] + options.center[2])
      << endl;
  }
  uint32_t nb_seg = (uint32_t) (subdivided.size() + middle_point_candidates.size());
  of << "CELLS " << nb_seg << " " << (3 * nb_seg) << endl;
  for (auto i = middle_point_candidates.begin();
            i != middle_point_candidates.end(); i++) {
    key_T key = i->first;
    if (subdivided.find(key) != subdivided.end()) {
      // this one is subdivided: 2 segments
      uint32_t mid = i->second.m_idx;
      of << "2 " << first(key) << " " << mid << endl;
      of << "2 " << mid << " " << second(key) << endl;
    }
    else {
      // this one is not
      of << "2 " << first(key) << " " << second(key) << endl;
    }
  }
  of << "CELL_TYPES " << nb_seg << endl;
  for (uint32_t i = 0; i < nb_seg; i++) {
    of << "3 ";
  }
  of << endl;
  of << "CELL_DATA " << nb_seg << endl;
  of << "SCALARS weights float" << endl;
  of << "LOOKUP_TABLE default" << endl;
  for (auto i = middle_point_candidates.begin();
            i != middle_point_candidates.end(); i++) {
    key_T key = i->first;
    float w = i->second.w;
    if (subdivided.find(key) != subdivided.end()) {
      // this one is subdivided
      of << w << endl;
      of << w << endl;
    }
    else {
      // this one is not
      of << w << endl;
    }
  }
  of.close();
}

// create an geodesic sphere centered at 0,0,0
// and displace the points using param_fn
void
create_geodesic_spheroid
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   param_fn_T param_fn,
   const param_options_T& options)
{
  // fake radius (r=1)
  create_geodesic_sphere(vertices, triangles, 1, options.level, options.mesh_orientation, options.convention);
  // actual radius
  for (uint32_t i = 0; i < (uint32_t) vertices->size(); i++) {
    vrtx_T& v = vertices->operator[](i);
    compute_vertex(&v, param_fn);
  }
}

// create an icosphere centered at 0,0,0
// and displace the points using param_fn
void
create_icospheroid_0
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   param_fn_T param_fn,
   const param_options_T& options)
{
  // fake radius (r=1)
  create_icosphere_0(vertices, triangles, 1, options.mesh_orientation, options.convention);
  // actual radius
  for (uint32_t i = 0; i < (uint32_t) vertices->size(); i++) {
    vrtx_T& v = vertices->operator[](i);
    compute_vertex(&v, param_fn);
  }
}

// choose the best option to split a triangle in 3 subtriangles
bool
first_is_best_for_3_trg_case
  (middle_pt_idx_map_T::iterator& ref1,
   middle_pt_idx_map_T::iterator& ref2,
   param_options_T* options)
{
  if ((ref1->second.crest_w >= options->crest_line_threshold)
    != (ref2->second.crest_w >= options->crest_line_threshold)) {
    // single crest line
    // don't cross a crest line
    return  ref1->second.crest_w < options->crest_line_threshold;
  }
  if (ref1->second.crest_w >= options->crest_line_threshold) {
    // 2 crest lines
    // chose to cross the less sharp crest
    return ref1->second.crest_w < ref2->second.crest_w;
  }
  // chose to create the closest-to-the-function edge
  if (ref1->second.w * ref2->second.w >= 0) {
    // both valid or both invalid
    // if both invalid we must chose one anyway to split the quadrilateral
    return ref1->second.w < ref2->second.w;
  }
  else {
    // the only one that is valid
    return ref1->second.w >= 0;
  }
}

// create the mesh
static void
make_param_mesh
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   param_fn_T param_fn,
   param_options_T* options)
{
  vector<trg_T> new_trgs;
  // nb of uniform subdivision steps for initial icosphere
  const int NB_UNIFORM_STEPS = 1;

  vector<trg_T> * dest = (options->refinement_policy == ADAPTATIVE) ? &new_trgs : triangles;

  // create the initial spheroid (refinement level 0)
  switch (options->base_sphere_type) {
  case GEODESIC: {
    create_geodesic_spheroid(vertices, dest, param_fn, *options);
    break;
  }
  case ICOSPHERE: {
    create_icospheroid_0(vertices, dest, param_fn, *options);

    int nb_steps =
      (options->refinement_policy == ADAPTATIVE)
      ? NB_UNIFORM_STEPS
      : NB_UNIFORM_STEPS + options->level;
    for (int i = 0; i < nb_steps; i++) {
      one_step_diadic_refine(vertices, dest, options->convention, param_fn);
    }
    break;
  }
  default: ASSERT(false);
  }

  if (options->refinement_policy == ADAPTATIVE) {
    sampling_path_T* path_ptr;
    sampling_path_T path;
    middle_pt_idx_map_T middle_point_candidates;

    if (options->output_refinement_steps) {
      // dump the initial icosphere
      dump_current_mesh_to_vtk(*options, 0, *vertices, new_trgs);
      path_ptr = &path;
    }
    else {
      path_ptr = nullptr;
    }

    // need to be relative to sample's size in some way
    float c = 0.025f * log(1 + (float) options->initial_samples_per_radian);
    options->crest_line_threshold = rate_crest_line(-c, c);

    // try to place the initial vertices at better spots
    int moved = 0;
    const float two_stp = (float) pow(2, NB_UNIFORM_STEPS);
    for (uint32_t i = 0; i < (uint32_t) vertices->size(); i++) {
      vrtx_T& pt = vertices->operator[](i);
      // TODO: find a way to move poles to a better place
      bool changed = false;
      bool efficient = false;
      // the displaced points must not overlap!
      float t_range;
      float p_range;
      if (options->base_sphere_type == ICOSPHERE) {
        t_range = 0.46f / two_stp;
        p_range = 0.625f / two_stp;
        ASSERT(0 < t_range * two_stp && t_range * two_stp < atan(0.5f)); // 0.463
        ASSERT(0 < p_range * two_stp && p_range * two_stp < (float) (0.2 * M_PI)); // 0.628
      }
      else {
        const uint32_t nb_ring = GEO_NB_RINGS(options->level);
        const uint32_t nb_slice = GEO_NB_SLICES(options->level);
        const float dphi = (float) (2 * M_PI) / (float) nb_slice;
        const float dtheta = (float) M_PI / (float) (nb_ring - 1);
        t_range = 0.95f * dtheta;
        p_range = 0.95f * dphi;
      }
      float range_limit = max(t_range, p_range) *
        (float) options->initial_samples_per_radian / (float) options->final_samples_per_radian;
      do {
        vrtx_T from, to, res;
        efficient = false;
        // try to improve the placement along a theta 'line'
        // (not for the poles)
        if (!is_pole(pt, options->convention)) {
          // same theta
          from.theta = to.theta = pt.theta;
          from.phi = clamp_phi(pt.phi - p_range * 0.5f, options->convention);
          to.phi = clamp_phi(pt.phi + p_range * 0.5f, options->convention);
          compute_vertex(&from, param_fn);
          compute_vertex(&to, param_fn);
          if (0 < locate_starting_point(from, to, param_fn, *options, &res, path_ptr)) {
            // we don't want to move the point at a range's boundary
            // we are looking for crest lines
            efficient = true;
            changed = true;
            pt = res;
          }
          else {
            // restart with pt for phi
          }
        }
        // try to improve the placement along a phi 'line'
        // (not for the poles)
        if (!is_pole(pt, options->convention)) {
          // same theta
          from.phi = to.phi = pt.phi;
          from.theta = clamp_theta(pt.theta - t_range * 0.5f, options->convention);
          to.theta = clamp_theta(pt.theta + t_range * 0.5f, options->convention);
          compute_vertex(&from, param_fn);
          compute_vertex(&to, param_fn);
          if (0 < locate_starting_point(from, to, param_fn, *options, &res, path_ptr)) {
            // we don't want to move the point at a range's boundary
            // we are looking for crest lines
            efficient = true;
            changed = true;
            pt = res;
          }
          else {
            // restart with pt for theta
          }
        }

        // restrict range
        t_range *= 0.5f;
        p_range *= 0.5f;
      } while (efficient && min(t_range, p_range) > range_limit);
      if (changed) {
        moved++;
      }
    }

    // compute scaled options
    for (uint32_t i = 0; i < (uint32_t) vertices->size(); i++) {
      const vrtx_T& pt = vertices->operator[](i);
      options->max_radius = max(options->max_radius, pt.r);
    }
    options->scaled_max_offset = options->max_radius * options->relative_error;
    options->scaled_min_edge
      = 2 * (float) M_PI * options->max_radius * options->relative_min_edge;
    options->scaled_min_4surf2
      = (float) pow((float) (2 * (float) M_PI * (float) pow(options->max_radius, 2) * options->relative_min_surf), 2);
    ASSERT(options->scaled_max_offset > 0);
    ASSERT(options->scaled_min_edge > 0);
    ASSERT(options->scaled_min_4surf2 > 0);

    if (options->output_refinement_steps) {
      dump_sampling_to_vtk(*options, 0, path);
    }

    // subdivide mesh
    uint32_t level = 0;
    while (++level <= (uint32_t) options->level) {
      if (options->output_refinement_steps) {
        path_ptr->clear();
        // dump the initial mesh
        dump_current_mesh_to_vtk(*options, level, *vertices, new_trgs);
      }

      // need to be relative to sample's size in some way
      float c = 0.01f * log(1 + (float) options->initial_samples_per_radian / (float) pow(2, level - 1));
      options->crest_line_threshold = rate_crest_line(-c, c);
      ASSERT(options->crest_line_threshold >= 0);

      // first build the list of edges
      set<key_T> edges;
      for (uint32_t t = 0; t < (uint32_t) new_trgs.size(); t++) {
        trg_T& tr = new_trgs[t];
        if (tr.locked) {
          continue;
        }
        float n[3], tmp1[3], tmp2[3];
        const float *a = vertices->operator[](tr.v[0]).pos;
        const float *b = vertices->operator[](tr.v[1]).pos;
        const float *c = vertices->operator[](tr.v[2]).pos;
        f3_cross(n, f3_sub(tmp1, b, a), f3_sub(tmp2, c, a));
        float _4surf2 = f3_dot(n, n);

        if (_4surf2 < options->scaled_min_4surf2) {
          tr.locked = true;
          continue;
        }

        for (int i = 0; i < 3; i++) {
          uint32_t idx1 = tr.v[i];
          uint32_t idx2 = tr.v[(i + 1) % 3];
          const float *pt1 = vertices->operator[](idx1).pos;
          const float *pt2 = vertices->operator[](idx2).pos;
          float tmp[3];
          f3_sub(tmp, pt1, pt2);
          float edge_len2 = f3_dot(tmp, tmp);
          key_T key = make_key(idx1, idx2);

          if (edge_len2 >= 4 * options->scaled_min_edge * options->scaled_min_edge) {
            edges.insert(key);
          }
        }
      }

      // produce the middle point candidates
      for (key_T key : edges) {
        // compute the vertex index of the middle point
        get_middle_point(param_fn, *options, vertices, &middle_point_candidates,
          key, path_ptr);
      }
      if (options->output_refinement_steps) {
        dump_sampling_to_vtk(*options, level, path);
      }

      // detect edges that conflict with edges
      // (cannot split 3 edges on a triangle with crests)
      // the visit order can change the result
      for (uint32_t t = 0; t < (uint32_t) new_trgs.size(); t++) {
        trg_T& tr = new_trgs[t];
        if (tr.locked) {
#if TRACES_2_COUT
          cout << "Triangle " << tr.v[0] << " " << tr.v[1] << " " << tr.v[2] << " is locked" << endl;
#endif
          continue;
        }
        int nb_possible_crests = 0;
        int nb_conflicting_edges = 0;
        int nb_candidate_edges = 0;
        bool ref_exist[3];
        bool is_crest[3];
        float weight[3];
        float crest_weight[3];
        edge_info_T* ref[3];
        key_T key[3];

#if TRACES_2_COUT
        cout << "In triangle " << tr.v[0] << " " << tr.v[1] << " " << tr.v[2] << endl;
#endif

        for (int i = 0; i < 3; i++) {
          uint32_t p1 = tr.v[i];
          uint32_t p2 = tr.v[(i + 1) % 3];
          key[i] = make_key(p1, p2);
          auto tmp = middle_point_candidates.find(key[i]);
          ref_exist[i] = tmp != middle_point_candidates.end() && tmp->second.m_idx != numeric_limits<uint32_t>::max();
          if (ref_exist[i]) {
            nb_candidate_edges++;
            ref[i] = &tmp->second;
            weight[i] = ref[i]->w;
            crest_weight[i] = ref[i]->crest_w;
            is_crest[i] = crest_weight[i] > options->crest_line_threshold;
            ASSERT(weight[i] > 0 || crest_weight[i] > 0);

            if (ref[i]->crest_conflict_at_level == level) {
              nb_conflicting_edges++;

#if TRACES_2_COUT
              if (is_crest[i])
                cout << "Crest " << p1 << " " << p2 << " allready in conflict" << endl;
              else
                cout << "Edge " << p1 << " " << p2 << " allready in conflict" << endl;
#endif
            }
            else {
              if (is_crest[i])
              {
                nb_possible_crests++;
              }
            }
          }
        }
        ASSERT(0 <= nb_possible_crests && nb_possible_crests <= 3);
        ASSERT(0 <= nb_conflicting_edges && nb_conflicting_edges <= 3);
        ASSERT(0 <= nb_candidate_edges && nb_candidate_edges <= 3);
        ASSERT(nb_candidate_edges >= nb_possible_crests + nb_conflicting_edges);
        if (nb_possible_crests > 0) {
          // we don't want subdivide the 3 edges at the same time
          // or the crest(s) won't be actually extracted
          if (nb_possible_crests != 3) {
            // do nothing if 1 edge is allready excluded
            // (that is non-candidate or in-conflict)
            if ((3 - nb_candidate_edges) + nb_conflicting_edges == 0) {
              // if 2 crests detected, don't subdivide the other edge
              // if 1 keep only the best one
              float worst = numeric_limits<float>::max();
              int worst_idx = -1;
              for (int i = 0; i < 3; i++) {
                if (ref_exist[i] && !is_crest[i] && worst > weight[i]) {
                  worst_idx = i;
                  worst = weight[i];
                }
              }
              ASSERT(worst_idx != -1);
              ref[worst_idx]->crest_conflict_at_level = level;

#if TRACES_2_COUT
              if (is_crest[worst_idx])
                cout << "Crest " << first(key[worst_idx]) << " " << second(key[worst_idx]) << " in conflict" << endl;
              else
                cout << "Edge " << first(key[worst_idx]) << " " << second(key[worst_idx]) << " in conflict" << endl;
#endif
            }
          }
          else {
            // select the best 2 crests
            float min_w = weight[0];
            int worst_idx = 0;
            for (int i = 1; i < 3; i++) {
              min_w = min(min_w, weight[i]);
              worst_idx = i;
            }
            ref[worst_idx]->crest_conflict_at_level = level;

#if TRACES_2_COUT
            cout << "Crest " << first(key[worst_idx]) << " " << second(key[worst_idx]) << " in conflict" << endl;
#endif
          }
        }
      }

      // then loop over edges to compute stats
      float nb_conflict = 0;
      float nb_crest = 0;
      float nb_non_crest = 0;
      float sweight = 0;
      float sweight2 = 0;
      for (auto edge = edges.begin(); edge != edges.end(); edge++) {
        key_T key = *edge;
        auto ref = middle_point_candidates.find(key);
        if (ref != middle_point_candidates.end()
          && ref->second.m_idx != numeric_limits<uint32_t>::max())
        {
          if (ref->second.crest_conflict_at_level == level) {
            nb_conflict++;
            continue;
          }
          bool is_crest = ref->second.crest_w > options->crest_line_threshold;
          if (is_crest) {
            nb_crest++;
          }
          else {
            // stats are only based on non-conflicting non-crest edges
            nb_non_crest++;
            float weight = ref->second.w;
            ASSERT(weight > 0);
            sweight += weight;
            sweight2 += weight * weight;
          }
        }
      }
      float av_weight = sweight / nb_non_crest;
      float stddev = sqrt(max(0.f, sweight2 / nb_non_crest - av_weight * av_weight));
      // compute threshold
      float weight_threshold0 = av_weight - 0.15f * stddev;
      float  weight_threshold1 = av_weight - 0.4f * stddev;
      float  weight_threshold2 = 0;

#if TRACES_2_COUT
      cout << endl << "Weight threshold is " << weight_threshold0 << endl;
      cout << middle_point_candidates.size() << " candidates" << endl;
#endif

      // loop over edges to choose which ones to subdivide
      for (auto edge = edges.begin(); edge != edges.end(); edge++) {
        key_T key = *edge;
        auto ref = middle_point_candidates.find(key);
        if (ref != middle_point_candidates.end()
          && ref->second.m_idx != numeric_limits<uint32_t>::max())
        {
          if (ref->second.crest_conflict_at_level == level) {
            ref->second.selectable = 3;
            continue;
          }
          float weight = ref->second.w;
          bool is_crest = ref->second.crest_w > options->crest_line_threshold;
          if (is_crest || weight >= weight_threshold0) {
            // will be selected
            ref->second.selectable = 0;
          }
          else if (weight >= weight_threshold1) {
            // could be selected if the 2 other edges where selected
            ref->second.selectable = 1;
          }
          else if (weight >= weight_threshold2) {
            // could be selected if 1 other edge was selected
            ref->second.selectable = 2;
          }
        }
      }

      // actual edges selection
      // first those without condition
      // seconf=d those that canbe selected only if a given number of 
      // selected edges is reached
      set<key_T> selected;
      for (uint32_t t = 0; t < (uint32_t) new_trgs.size(); t++) {
        trg_T& tr = new_trgs[t];
        bool ref_exist[3];
#if TRACES_2_COUT
        bool is_crest[3];
#endif
        edge_info_T* ref[3];
        key_T key[3];
        unsigned nb_selected = 0;
        for (int i = 0; i < 3; i++) {
          uint32_t p1 = tr.v[i];
          uint32_t p2 = tr.v[(i + 1) % 3];
          key[i] = make_key(p1, p2);
          auto tmp = middle_point_candidates.find(key[i]);
          ref_exist[i] = tmp != middle_point_candidates.end();

          if (ref_exist[i]) {
            ref[i] = &tmp->second;
#if TRACES_2_COUT
            is_crest[i] = ref[i]->crest_w > options->crest_line_threshold;
#endif
            if (tmp->second.selectable == 0
              && tmp->second.crest_conflict_at_level != level)
            {
              // was just waiting for non crest checking
              selected.insert(key[i]);
              nb_selected++;

#if TRACES_2_COUT
              float weight = ref[i]->w;
              float crest_weight = ref[i]->crest_w;
              uint32_t p1 = tr.v[i];
              uint32_t p2 = tr.v[(i + 1) % 3]; 
              if (is_crest[i]) {
                cout << "Crest " << p1 << " " << p2 << " selected; w="
                  << crest_weight << "(" << ((int) ref[i]->selectable) << ")" << endl;
              }
              else {
                cout << "Edge " << p1 << " " << p2 << " selected; w="
                  << weight << "(" << ((int) ref[i]->selectable) << ")" << endl;
              }
#endif
            }
          }
        }
        ASSERT(nb_selected <= 3);
        if (nb_selected == 1 || nb_selected == 2) {
          for (int i = 0; i < 3; i++) {
            if (ref_exist[i]
              && ref[i]->selectable != 0
              && ref[i]->crest_conflict_at_level != level
              && ref[i]->selectable <= nb_selected)
            {
              // was waiting for a second chance
              selected.insert(key[i]);

#if TRACES_2_COUT
              float weight = ref[i]->w;
              float crest_weight = ref[i]->crest_w;
              uint32_t p1 = tr.v[i];
              uint32_t p2 = tr.v[(i + 1) % 3];
              if (is_crest[i]) {
                cout << "Crest " << p1 << " " << p2 << " selected; w="
                  << crest_weight << "(" << ((int) ref[i]->selectable) << ")" << endl;
              }
              else {
                cout << "Edge " << p1 << " " << p2 << " selected; w="
                  << weight << "(" << ((int) ref[i]->selectable) << ")" << endl;
              }
#endif
            }
          }
        }
      }

#ifndef NDEBUG
      // check selected list coherency
      for (auto edge = selected.begin(); edge != selected.end(); edge++) {
        key_T key = *edge;
        auto ref = middle_point_candidates.find(key);
        ASSERT(ref != middle_point_candidates.end());
        ASSERT(ref->second.crest_conflict_at_level != level);
        ASSERT(ref->second.m_idx < vertices->size());
      }
#endif

#if TRACES_2_COUT
      cout << endl << selected.size() << " edges selected" << endl;
#endif

      if (options->output_refinement_steps) {
        dump_subdivided_edges_to_vtk
          (*options, level, middle_point_candidates, *vertices, selected);
      }
      // check what subdivision pattern applies to each triangle and subdivide it
      vector<trg_T> refine_trgs;
      for (uint32_t t = 0; t < (uint32_t) new_trgs.size(); t++) {
        trg_T& tr = new_trgs[t];
        unsigned needed_msk = 0;
        for (int i = 0; i < 3; i++) {
          uint32_t p1 = tr.v[i];
          uint32_t p2 = tr.v[(i + 1) % 3];
          key_T key = make_key(p1, p2);
          needed_msk *= 2;
          if (selected.find(key) != selected.end()) {
            needed_msk++;
            ASSERT(middle_point_candidates.find(key) != middle_point_candidates.end());
            ASSERT(middle_point_candidates.find(key)->second.crest_conflict_at_level != level);
            ASSERT(middle_point_candidates.find(key)->second.m_idx < vertices->size());
          }
        }
        ASSERT(needed_msk <= 7);
        // the 1, 2 and 4 triangles are simple as there is 1 single way to subdivide
        // the 3 triangles case is more tricky and a choise has to be made
        // we chose to favor the creation of the edge
        // that will less likely be further subdivided
        switch (needed_msk) {
        case 0: {
          // no more subdivision needed, but keep the triangle
          //tr.locked = true;
          add_trg(&refine_trgs, tr);
          break;
        }
        case 1: {
          // need 2 triangles
          key_T key = make_key(tr.v[2], tr.v[0]);
          auto ref = middle_point_candidates.find(key);
          ASSERT(ref != middle_point_candidates.end());
          ASSERT(ref->second.m_idx < vertices->size());
          uint32_t mid_2_0 = ref->second.m_idx;
          ASSERT(mid_2_0 < vertices->size());
          add_trg(&refine_trgs, trg_T(tr.v[0], tr.v[1], mid_2_0));
          add_trg(&refine_trgs, trg_T(tr.v[1], tr.v[2], mid_2_0));
          break;
        }
        case 2: {
          // need 2 triangles
          key_T key = make_key(tr.v[1], tr.v[2]);
          auto ref = middle_point_candidates.find(key);
          ASSERT(ref != middle_point_candidates.end());
          ASSERT(ref->second.m_idx < vertices->size());
          uint32_t mid_1_2 = ref->second.m_idx;
          ASSERT(mid_1_2 < vertices->size());
          add_trg(&refine_trgs, trg_T(tr.v[0], tr.v[1], mid_1_2));
          add_trg(&refine_trgs, trg_T(tr.v[0], mid_1_2, tr.v[2]));
          break;
        }
        case 4: {
          // need 2 triangles
          key_T key = make_key(tr.v[0], tr.v[1]);
          auto ref = middle_point_candidates.find(key);
          ASSERT(ref != middle_point_candidates.end());
          ASSERT(ref->second.m_idx < vertices->size());
          uint32_t mid_0_1 = ref->second.m_idx;
          ASSERT(mid_0_1 < vertices->size());
          add_trg(&refine_trgs, trg_T(tr.v[0], mid_0_1, tr.v[2]));
          add_trg(&refine_trgs, trg_T(tr.v[1], tr.v[2], mid_0_1));
          break;
        }
        case 3: {
          // need 3 triangles
          key_T key1 = make_key(tr.v[1], tr.v[2]);
          key_T key2 = make_key(tr.v[2], tr.v[0]);
          auto ref1 = middle_point_candidates.find(key1);
          ASSERT(ref1 != middle_point_candidates.end());
          auto ref2 = middle_point_candidates.find(key2);
          ASSERT(ref2 != middle_point_candidates.end());
          uint32_t mid_1_2 = ref1->second.m_idx;
          uint32_t mid_2_0 = ref2->second.m_idx;
          ASSERT(mid_1_2 < vertices->size());
          ASSERT(mid_2_0 < vertices->size());
          // get the vertex score of the middle point
          // or -FLT_MAX if no split is computed
          key_T keyA = make_key(tr.v[0], mid_1_2);
          key_T keyB = make_key(tr.v[1], mid_2_0);
          ASSERT(middle_point_candidates.find(keyA) == middle_point_candidates.end());
          ASSERT(middle_point_candidates.find(keyB) == middle_point_candidates.end());
          auto refA = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyA, path_ptr);
          ASSERT(middle_point_candidates.find(keyA) == refA);
          ASSERT(refA != middle_point_candidates.end());
          auto refB = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyB, path_ptr);
          ASSERT(middle_point_candidates.find(keyB) == refB);
          ASSERT(refB != middle_point_candidates.end());

          // this triangle must be created
          add_trg(&refine_trgs, trg_T(tr.v[2], mid_2_0, mid_1_2));
          // create the 2 triangles for which the likelihood
          // of subsequent subdivision is minimal
          if (first_is_best_for_3_trg_case(refA, refB, options)) {
            add_trg(&refine_trgs, trg_T(tr.v[0], tr.v[1], mid_1_2));
            add_trg(&refine_trgs, trg_T(tr.v[0], mid_1_2, mid_2_0));
          }
          else {
            // its a bad news if none of the 2 candidate triangles has >0 score
            // but we have to split the quadrangle, so we proceed anyway
            // if the ASSERT rings, think of an alternative
            add_trg(&refine_trgs, trg_T(tr.v[0], tr.v[1], mid_2_0));
            add_trg(&refine_trgs, trg_T(tr.v[1], mid_1_2, mid_2_0));
          }
          break;
        }
        case 5: {
          // need 3 triangles
          key_T key1 = make_key(tr.v[0], tr.v[1]);
          key_T key2 = make_key(tr.v[2], tr.v[0]);
          auto ref1 = middle_point_candidates.find(key1);
          ASSERT(ref1 != middle_point_candidates.end());
          auto ref2 = middle_point_candidates.find(key2);
          ASSERT(ref2 != middle_point_candidates.end());
          uint32_t mid_0_1 = ref1->second.m_idx;
          uint32_t mid_2_0 = ref2->second.m_idx;
          ASSERT(mid_0_1 < vertices->size());
          ASSERT(mid_2_0 < vertices->size());
          // get the vertex score of the middle point
          // or -FLT_MAX if no split is computed
          key_T keyA = make_key(tr.v[1], mid_2_0);
          key_T keyB = make_key(tr.v[2], mid_0_1);
          ASSERT(middle_point_candidates.find(keyA) == middle_point_candidates.end());
          ASSERT(middle_point_candidates.find(keyB) == middle_point_candidates.end());
          auto refA = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyA, path_ptr);
          ASSERT(middle_point_candidates.find(keyA) == refA);
          ASSERT(refA != middle_point_candidates.end());
          auto refB = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyB, path_ptr);
          ASSERT(middle_point_candidates.find(keyB) == refB);
          ASSERT(refB != middle_point_candidates.end());

          // this triangle must be created
          add_trg(&refine_trgs, trg_T(tr.v[0], mid_0_1, mid_2_0));
          // create the 2 triangles for which the likelihood
          // of subsequent subdivision is minimal
          if (first_is_best_for_3_trg_case(refA, refB, options)) {
            add_trg(&refine_trgs, trg_T(tr.v[1], mid_2_0, mid_0_1));
            add_trg(&refine_trgs, trg_T(tr.v[2], mid_2_0, tr.v[1]));
          }
          else {
            // its a bad news if none of the 2 candidate triangles has >0 score
            // but we have to split the quadrangle, so we proceed anyway
            // if the ASSERT rings, think of an alternative
            add_trg(&refine_trgs, trg_T(tr.v[1], tr.v[2], mid_0_1));
            add_trg(&refine_trgs, trg_T(tr.v[2], mid_2_0, mid_0_1));
          }
          break;
        }
        case 6: {
          // need 3 triangles
          key_T key1 = make_key(tr.v[0], tr.v[1]);
          key_T key2 = make_key(tr.v[1], tr.v[2]);
          auto ref1 = middle_point_candidates.find(key1);
          ASSERT(ref1 != middle_point_candidates.end());
          auto ref2 = middle_point_candidates.find(key2);
          ASSERT(ref2 != middle_point_candidates.end());
          uint32_t mid_0_1 = ref1->second.m_idx;
          uint32_t mid_1_2 = ref2->second.m_idx;
          ASSERT(mid_0_1 < vertices->size());
          ASSERT(mid_1_2 < vertices->size());
          // get the vertex score of the middle point
          // or -FLT_MAX if no split is computed
          key_T keyA = make_key(tr.v[0], mid_1_2);
          key_T keyB = make_key(mid_0_1, tr.v[2]);
          ASSERT(middle_point_candidates.find(keyA) == middle_point_candidates.end());
          ASSERT(middle_point_candidates.find(keyB) == middle_point_candidates.end());
          auto refA = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyA, path_ptr);
          ASSERT(middle_point_candidates.find(keyA) == refA);
          ASSERT(refA != middle_point_candidates.end());
          auto refB = get_middle_point(param_fn, *options, vertices,
            &middle_point_candidates, keyB, path_ptr);
          ASSERT(middle_point_candidates.find(keyB) == refB);
          ASSERT(refB != middle_point_candidates.end());

          // this triangle must be created
          add_trg(&refine_trgs, trg_T(tr.v[1], mid_1_2, mid_0_1));
          // create the 2 triangles for which the likelihood
          // of subsequent subdivision is minimal
          if (first_is_best_for_3_trg_case(refA, refB, options)) {
            add_trg(&refine_trgs, trg_T(tr.v[0], mid_0_1, mid_1_2));
            add_trg(&refine_trgs, trg_T(tr.v[2], tr.v[0], mid_1_2));
          }
          else {
            // its a bad news if none of the 2 candidate triangles has >0 score
            // but we have to split the quadrangle, so we proceed anyway
            // if the ASSERT rings, think of an alternative
            add_trg(&refine_trgs, trg_T(tr.v[0], mid_0_1, tr.v[2]));
            add_trg(&refine_trgs, trg_T(tr.v[2], mid_0_1, mid_1_2));
          }
          break;
        }
        case 7: {
          // need 4 triangles
          key_T key1 = make_key(tr.v[0], tr.v[1]);
          key_T key2 = make_key(tr.v[1], tr.v[2]);
          key_T key3 = make_key(tr.v[2], tr.v[0]);
          auto ref1 = middle_point_candidates.find(key1);
          ASSERT(ref1 != middle_point_candidates.end());
          auto ref2 = middle_point_candidates.find(key2);
          ASSERT(ref2 != middle_point_candidates.end());
          auto ref3 = middle_point_candidates.find(key3);
          ASSERT(ref3 != middle_point_candidates.end());
          uint32_t mid_0_1 = ref1->second.m_idx;
          uint32_t mid_1_2 = ref2->second.m_idx;
          uint32_t mid_2_0 = ref3->second.m_idx;
          ASSERT(mid_0_1 < vertices->size());
          ASSERT(mid_1_2 < vertices->size());
          ASSERT(mid_2_0 < vertices->size());
          add_trg(&refine_trgs, trg_T(tr.v[0], mid_0_1, mid_2_0));
          add_trg(&refine_trgs, trg_T(tr.v[1], mid_1_2, mid_0_1));
          add_trg(&refine_trgs, trg_T(tr.v[2], mid_2_0, mid_1_2));
          add_trg(&refine_trgs, trg_T(mid_0_1, mid_1_2, mid_2_0));
          break;
        }
        }
      }
      new_trgs = refine_trgs;
      // remove selected edges from middle_points_indexes
      // keep the others for future use
      //for (auto i = selected.begin(); i != selected.end(); i++) {
      //  middle_point_candidates.erase(*i);
      //}
    }

    // keep triangles
    ASSERT(triangles->size() == 0);
    *triangles = new_trgs;
  }

  if (options->output_refinement_steps) {
    // dump the final mesh
    dump_current_mesh_to_vtk(*options, options->level, *vertices, *triangles);
  }

  // the mesh is ready, but centered at <0,0,0>: offset vertices
  for (size_t i = 0; i < vertices->size(); i++) {
    float *pt = vertices->operator[](i).pos;
    for (int j = 0; j < 3; j++) {
      pt[j] += options->center[j];
    }
  }

  // dump the final mesh
  if (options->output_final_mesh) {
    dump_mesh_to_obj
      (options->base_name + "param.obj", *vertices, *triangles);
  }
}

 /*******************************************************************************
 * Exported s3du_shape functions:
 ******************************************************************************/
res_T
s3du_shape_mesh_param_function
  (struct s3d_device* dev,
   struct s3d_shape** out_is,
   param_fn_T param_fn,
   const struct param_mesh_options_T* _options)
{
  struct s3d_shape* is = nullptr;
  struct s3d_vertex_data attrib;
  vector<vrtx_T> vertices;
  vector<trg_T> triangles;
  user_data_T udata;
  struct param_options_T options;
  res_T res = RES_OK;

  if (!dev || !out_is) {
    res = RES_BAD_ARG;
    goto error;
  }

  if (param_fn == 0) {
    res = RES_BAD_ARG;
    goto error;
  }

  if (0 >= _options->relative_error
    || _options->relative_error >= 1
    || 0 >= _options->relative_min_edge
    || 0 >= _options->initial_samples_per_radian
    || 0 >= _options->final_samples_per_radian)
  {
    res = RES_BAD_ARG;
    goto error;
  }

  options.param_mesh_options_T::operator=(*_options);
  if (options.output_refinement_steps || options.output_final_mesh) {
    time_to_string(&options.base_name, options.output_files_base_name);
  }

  res = s3d_shape_create_mesh(dev, &is);
  if (res != RES_OK) {
    goto error;
  }
  
  try {
    make_param_mesh(&vertices, &triangles, param_fn, &options);
  }
  catch (std::bad_alloc) {
    res = RES_MEM_ERR;
  }
  catch (...) {
    ASSERT(false); // not supposed to occur!
    res = RES_BAD_OP;
  }
  
  if (res != RES_OK) {
    goto error;
  }

  attrib.type = S3D_FLOAT3;
  attrib.usage = S3D_POSITION;
  attrib.get = _get_position;

  udata.vrtcs = &vertices; 
  udata.trgs = &triangles;
  res = s3d_mesh_setup_indexed_vertices(is, (unsigned) triangles.size(),
    _get_ids, (unsigned) vertices.size(), &attrib, 1, &udata);
  if (res != RES_OK) {
    goto error;
  }

exit:
  if (out_is)
    *out_is = is;
  return res;

error:
  if (is) {
    S3D(shape_ref_put(is));
    is = nullptr;
  }
  goto exit;
}

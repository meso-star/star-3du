/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software. You can use,
* modify and/or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info".
*
* As a counterpart to the access to the source code and rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty and the software's author, the holder of the
* economic rights, and the successive licensors have only limited
* liability.
*
* In this respect, the user's attention is drawn to the risks associated
* with loading, using, modifying and/or developing or reproducing the
* software by the user in light of its specific status of free software,
* that may mean that it is complicated to manipulate, and that also
* therefore means that it is reserved for developers and experienced
* professionals having in-depth computer knowledge. Users are therefore
* encouraged to load and test the software's suitability as regards their
* requirements in conditions enabling the security of their systems and/or
* data to be ensured and, more generally, to use and operate it in the
* same conditions as regards security.
*
* The fact that you are presently reading this means that you have had
* knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"
#include "s3du_common.h"

#include <star/s3d.h>

#include <stdexcept>
#define _USE_MATH_DEFINES
#include <math.h>

using std::pow;
using std::exception;

/******************************************************************************
* default options
******************************************************************************/

const struct spherical_mesh_options_T default_spherical_mesh_options = {
  { default_center[0], default_center[1], default_center[2] },
  default_sphere_type,
  default_level,
  default_mesh_orientation,
  default_output_final_mesh,
  default_output_files_base_name
};

/******************************************************************************
* extended type for options
******************************************************************************/

struct sphere_options_T : public spherical_mesh_options_T {
  parametrization_T convention;
  string base_name;
};

/******************************************************************************
* functions
******************************************************************************/

static void
make_spherical_mesh
  (vector<vrtx_T>* vertices,
   vector<trg_T>* triangles,
   float radius,
   const sphere_options_T& options)
{
  switch (options.type) {
  case ICOSPHERE: {
    // total number of triangles
    unsigned int nbt = 20 * (unsigned int) pow(4, options.level);
    // total number of vertices
    unsigned int nbv = 2 + nbt / 2;
    vertices->reserve(nbv);
    triangles->reserve(nbt);

    create_icosphere_0(vertices, triangles, radius, options.mesh_orientation, options.convention);
    for (uint32_t i = 0; i < options.level; i++) {
      one_step_diadic_refine(vertices, triangles, options.convention, nullptr, radius);
    }

    ASSERT(vertices->size() == nbv);
    ASSERT(triangles->size() == nbt);
    break;
  }
  case GEODESIC: {
    const uint32_t nb_ring = GEO_NB_RINGS(options.level);
    const uint32_t nb_slice = GEO_NB_SLICES(options.level);
    // total number of triangles
    unsigned int nbt = 2 * nb_slice + 2 * (nb_ring - 3) * nb_slice;
    // total number of vertices
    unsigned int nbv = 2 + (nb_ring - 2) * nb_slice;
    vertices->reserve(nbv);
    triangles->reserve(nbt);
    create_geodesic_sphere(vertices, triangles, radius, options.level, options.mesh_orientation, options.convention);

    ASSERT(vertices->size() == nbv);
    ASSERT(triangles->size() == nbt);
    break;
  }
  default: ASSERT(false);
  }

  // the mesh is ready, but centered at <0,0,0>: offset vertices
  for (size_t i = 0; i < vertices->size(); i++) {
    float *pt = vertices->operator[](i).pos;
    for (int j = 0; j < 3; j++) {
      pt[j] += options.center[j];
    }
  }

  // dump the final mesh
  if (options.output_final_mesh) {
    dump_mesh_to_obj
      (options.base_name + "sphere.obj", *vertices, *triangles);
  }
}

/*******************************************************************************
 * Exported s3du_shape functions:
 ******************************************************************************/

res_T
s3du_shape_create_sphere
  (struct s3d_device* dev,
   struct s3d_shape** out_pp,
   float radius,
   const struct spherical_mesh_options_T* _options)
{
  struct s3d_shape* pp = NULL;
  struct s3d_vertex_data attrib;
  vector<vrtx_T> vertices;
  vector<trg_T> triangles;
  user_data_T udata;
  sphere_options_T options;
  res_T res = RES_OK;

  if (!dev || !out_pp) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = s3d_shape_create_mesh(dev, &pp);
  if (res != RES_OK) {
    goto error;
  }

  options.spherical_mesh_options_T::operator=(*_options);
  if (options.output_final_mesh) {
    time_to_string(&options.base_name, options.output_files_base_name);
  }
  options.convention = SPHERICAL_1;

  try {
    make_spherical_mesh(&vertices, &triangles, radius, options);
  }
  catch (std::bad_alloc) {
    res = RES_MEM_ERR;
  }
  catch (...) {
    ASSERT(false); // not supposed to occur!
    res = RES_BAD_OP;
  }

  attrib.type = S3D_FLOAT3;
  attrib.usage = S3D_POSITION;
  attrib.get = _get_position;

  udata.vrtcs = &vertices;
  udata.trgs = &triangles;
  res = s3d_mesh_setup_indexed_vertices(pp, (unsigned) triangles.size(),
    _get_ids, (unsigned) vertices.size(), &attrib, 1, &udata);

  if (res != RES_OK) {
    goto error;
  }

exit:
  if (out_pp)
    *out_pp = pp;
  return res;

error:
  if (pp) {
    S3D(shape_ref_put(pp));
    pp = NULL;
  }
  goto exit;
}

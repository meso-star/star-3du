/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"

#include <star/s3d.h>

#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>

#include <math.h>
#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#endif

/* not defined in ANSI C */
#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

static void
check_memory_allocator(struct mem_allocator* allocator)
{
	if (MEM_ALLOCATED_SIZE(allocator)) {
		char dump[512];
		MEM_DUMP(allocator, dump, sizeof(dump) / sizeof(char));
		fprintf(stderr, "%s\n", dump);
		FATAL("Memory leaks\n");
	}
}

/* keep it simple: use global variables */

struct super {
  float m, n1, n2, n3, a, b;
};

static struct super s1;
static struct super s2;

#define R(s, angle)\
   (pow(pow(fabs(cos((s).m * (angle) / 4.0f) / (s).a), (s).n2)\
    + pow(fabs(sin((s).m * (angle) / 4.0f) / (s).b), (s).n3), -1.0f / (s).n1))

/* see http://paulbourke.net/geometry/supershape/ */
static float
superfunction(float latitude, float longitude, float pt[3]) {
  float r1, r2;
  /* latitude in [-pi/2 +pi/2] and longitude in [-pi +pi] */
  ASSERT(-0.5f * (float) M_PI <= latitude && latitude <= +0.5f * (float) M_PI);
  ASSERT(-(float) M_PI <= longitude && longitude <= +(float) M_PI);

  r1 = (float) R(s1, latitude);
  r2 = (float) R(s2, longitude);

  pt[0] = (float) (r1 * cos(longitude) * r2 * cos(latitude));
  pt[1] = (float) (r1 * sin(longitude) * r2 * cos(latitude));
  pt[2] = (float) (r1 * sin(latitude));

  return (float) sqrt(pt[0] * pt[0] + pt[1] * pt[1] + pt[2] * pt[2]);
}

struct tcheb {
  float a, n, eps;
};

static struct tcheb t;

static float
tchebytchev(float latitude, float longitude, float pt[3]) {
  float r;
  /* latitude in [0 pi] and longitude in [0 2pi] */
  ASSERT(0 <= latitude && latitude <= (float) M_PI);
  ASSERT(0 <= longitude && longitude <= (float) (2 * M_PI));

  r = (float) (t.a * (1 + t.eps * cos(t.n * latitude)));

  pt[0] = (float) (r * sin(latitude) * cos(longitude));
  pt[1] = (float) (r * sin(latitude) * sin(longitude));
  pt[2] = (float) (r * cos(latitude));

  return r;
}

static void
test_supershape(uint32_t level, float c[3]) {
  struct mem_allocator allocator;
  struct s3d_device* dev;
  struct s3d_scene* scn;
  struct s3d_shape* super;
  struct param_mesh_options_T options;
  struct time t0, t1;
  char buf[512];
  
  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHECK(s3d_device_create(NULL, &allocator, 0, &dev), RES_OK);
  CHECK(s3d_scene_create(dev, &scn), RES_OK);

  options = default_mesh_param_function_options;
  options.level = level;
  options.convention = SPHERICAL_2;
  f3_set(options.center, c);

#ifndef NDEBUG
  /* be sure that 2 tests have different time */
#ifdef WIN32
  Sleep(1000);
#else
  sleep(1);
#endif
  options.output_final_mesh = 1;
  /* options.output_refinement_steps = 1; */
  sprintf(buf, "cw_%s", default_output_files_base_name);
  options.output_files_base_name = buf;
  options.mesh_orientation = CW;
#endif

  time_current(&t0);
  CHECK(s3du_shape_mesh_param_function(dev, &super, superfunction, &options),
    RES_OK);
  time_current(&t1);
  time_sub(&t0, &t1, &t0);
  time_dump(&t0, TIME_SEC | TIME_MSEC | TIME_USEC, NULL, buf, sizeof(buf));
  printf(
    "Supershape [%g %g %g %g %g %g]x[%g %g %g %g %g %g] meshed at level %d in %s\n",
    s1.m, s1.n1, s1.n2, s1.n3, s1.a, s1.b,
    s2.m, s2.n1, s2.n2, s2.n3, s2.a, s2.b,
    level,
    buf
  );
  
  CHECK(s3d_scene_attach_shape(scn, super), RES_OK);

  CHECK(s3d_shape_ref_put(super), RES_OK);
  CHECK(s3d_device_ref_put(dev), RES_OK);
  CHECK(s3d_scene_ref_put(scn), RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHECK(mem_allocated_size(), 0);
}

static void
test_tchebytchev(uint32_t level, float c[3]) {
  struct mem_allocator allocator;
  struct s3d_device* dev;
  struct s3d_scene* scn;
  struct s3d_shape* tche;
  struct param_mesh_options_T options;
  struct time t0, t1;
  char buf[512];

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  CHECK(s3d_device_create(NULL, &allocator, 0, &dev), RES_OK);
  CHECK(s3d_scene_create(dev, &scn), RES_OK);

  options = default_mesh_param_function_options;
  options.level = level;
  f3_set(options.center, c);

#ifndef NDEBUG
  /* be sure that 2 tests have different time */
#ifdef WIN32
  Sleep(1000);
#else
  sleep(1);
#endif
  options.output_final_mesh = 1;
  /* options.output_refinement_steps = 1; */
  sprintf(buf, "cw_%s", default_output_files_base_name);
  options.output_files_base_name = buf;
  options.mesh_orientation = CW;
#endif

  time_current(&t0);
  CHECK(s3du_shape_mesh_param_function(dev, &tche, tchebytchev, &options),
    RES_OK);
  time_current(&t1);
  time_sub(&t0, &t1, &t0);
  time_dump(&t0, TIME_SEC | TIME_MSEC | TIME_USEC, NULL, buf, sizeof(buf));
  printf(
    "Tchebytchev particle [%g %g %g] meshed at level %d in %s\n",
    t.a, t.n, t.eps,
    level,
    buf
    );

  CHECK(s3d_scene_attach_shape(scn, tche), RES_OK);

  CHECK(s3d_shape_ref_put(tche), RES_OK);
  CHECK(s3d_device_ref_put(dev), RES_OK);
  CHECK(s3d_scene_ref_put(scn), RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHECK(mem_allocated_size(), 0);
}

int
main(int argc, char** argv)
{
  float c[3] = { 0, 0, 0 };
  uint32_t level;

  if (argc != 5 && argc != 8 && argc != 14 && argc != 17) {
    fprintf(
      stderr,
      "Usage 1 (supershape): %s m_1 n1_1 n2_1 n3_1 a_1 b_1 m_2 n1_2 n2_2 n3_2 a_2 b_2 level [x y z]\n",
      argv[0]
      );
    fprintf(
      stderr,
      "Usage 2 (tchebytchev): %s a n eps level [x y z]\n",
      argv[0]
      );
    exit(1);
  }

  if (argc == 5 || argc == 8) {
    /* keep things simple: use a global variable */
    t.a = (float) atof(argv[1]);
    t.n = (float) atof(argv[2]);
    t.eps = (float) atof(argv[3]);

    level = (uint32_t) atoi(argv[4]);

    if (argc == 8) {
      c[0] = (float) atof(argv[5]);
      c[1] = (float) atof(argv[6]);
      c[2] = (float) atof(argv[7]);
    }

    test_tchebytchev(level, c);
  }
  else {
    /* keep things simple: use global variables */
    s1.m = (float) atof(argv[1]);
    s1.n1 = (float) atof(argv[2]);
    s1.n2 = (float) atof(argv[3]);
    s1.n3 = (float) atof(argv[4]);
    s1.a = (float) atof(argv[5]);
    s1.b = (float) atof(argv[6]);

    s2.m = (float) atof(argv[7]);
    s2.n1 = (float) atof(argv[8]);
    s2.n2 = (float) atof(argv[9]);
    s2.n3 = (float) atof(argv[10]);
    s2.a = (float) atof(argv[11]);
    s2.b = (float) atof(argv[12]);

    level = (uint32_t) atoi(argv[13]);

    if (argc == 17) {
      c[0] = (float) atof(argv[14]);
      c[1] = (float) atof(argv[15]);
      c[2] = (float) atof(argv[16]);
    }

    test_supershape(level, c);
  }
  return 0;
}

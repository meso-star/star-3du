/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"

#include <star/s3d.h>

#include <rsys/float3.h>
#include <rsys/mem_allocator.h>
#include <rsys/clock_time.h>
#include <rsys/clock_time.h>

#define _USE_MATH_DEFINES
#include <math.h>
#include <string.h>
#include <ctype.h>

static void
check_memory_allocator(struct mem_allocator* allocator)
{
	if (MEM_ALLOCATED_SIZE(allocator)) {
		char dump[512];
		MEM_DUMP(allocator, dump, sizeof(dump) / sizeof(char));
		fprintf(stderr, "%s\n", dump);
		FATAL("Memory leaks\n");
	}
}


static void
test_sphere(float radius, unsigned level, enum sphere_type_T type, float center[3]) {
  struct mem_allocator allocator;
  struct s3d_device* dev;
  struct s3d_scene* scn;
  struct s3d_shape* sphere;
  struct spherical_mesh_options_T options;
  struct time t0, t1;
  char buf[512];
  
  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  s3d_device_create(NULL, &allocator, 1, &dev);
  s3d_scene_create(dev, &scn);
  
  options = default_spherical_mesh_options;
  options.level = level;
  options.type = type;
  f3_set(options.center, center);

#ifndef NDEBUG
  options.output_final_mesh = 1;
#endif

  time_current(&t0);
  CHECK(s3du_shape_create_sphere(dev, &sphere, radius, &options), RES_OK);
  time_current(&t1);
  time_sub(&t0, &t1, &t0);
  time_dump(&t0, TIME_SEC | TIME_MSEC | TIME_USEC, NULL, buf, sizeof(buf));
  printf("Sphere meshed at level %d in %s\n", level, buf);
  
  CHECK(s3d_scene_attach_shape(scn, sphere), RES_OK);
  
  CHECK(s3d_shape_ref_put(sphere), RES_OK);
  CHECK(s3d_device_ref_put(dev), RES_OK);
  CHECK(s3d_scene_ref_put(scn), RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHECK(mem_allocated_size(), 0);
}

int
main(int argc, char** argv)
{
  enum sphere_type_T type = GEODESIC;
  unsigned level = 2;
  float radius = 1;
  float center[3] = { 0.5f, 0.5f, 0 };
  int i;

  if (argc > 4 && argc != 7)
    goto err;

  if (argc >= 2) {
    char buff[4];
    strncpy(buff, argv[1], 4);
    for (i = 0; i < 4; i++) 
      buff[i] = (char)toupper(buff[i]);
    if (!strcmp("ICO", buff))
      type = ICOSPHERE;
    else if (!strcmp("GEO", buff))
      type = GEODESIC;
    else goto err;
  }
  if (argc >= 3) {
    radius = (float) atof(argv[2]);
  }
  if (argc >= 4) {
    level = (unsigned)atoi(argv[3]);
  }
  if (argc == 7) {
    center[0] = (float) atof(argv[4]);
    center[1] = (float) atof(argv[5]);
    center[2] = (float) atof(argv[6]);
  }
  test_sphere(radius, level, type, center);

  return 0;
err:
  fprintf(stderr, "Usage: %s [ICO|GEO [radius [level [x y z]]]]\n", argv[0]);
  exit(1);
}

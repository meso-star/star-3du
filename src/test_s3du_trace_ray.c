/* Copyright (C) |Meso|Star> 2015 (contact@meso-star.com)
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms. */

#include "s3du.h"

#include <star/s3d.h>

#include <rsys/image.h>
#include <rsys/float3.h>
#include <rsys/mem_allocator.h>

static void
check_memory_allocator(struct mem_allocator* allocator)
{
	if (MEM_ALLOCATED_SIZE(allocator)) {
		char dump[512];
		MEM_DUMP(allocator, dump, sizeof(dump) / sizeof(char));
		fprintf(stderr, "%s\n", dump);
		FATAL("Memory leaks\n");
	}
}

#define IMG_WIDTH 640
#define IMG_HEIGHT 480

struct camera {
  float pos[3];
  float x[3], y[3], z[3]; /* Basis */
};

static void
camera_init(struct camera* cam)
{
  const float pos[3] = { 178.f, -1000.f, 273.f };
  const float tgt[3] = { 178.f, 0.f, 273.f };
  const float up[3] = { 0.f, 0.f, 1.f };
  const float proj_ratio = (float)IMG_WIDTH/(float)IMG_HEIGHT;
  const float fov_x = (float)PI * 0.25f;
  float f = 0.f;
  ASSERT(cam);

  f3_set(cam->pos, pos);
  f = f3_normalize(cam->z, f3_sub(cam->z, tgt, pos)); NCHECK(f, 0);
  f = f3_normalize(cam->x, f3_cross(cam->x, cam->z, up)); NCHECK(f, 0);
  f = f3_normalize(cam->y, f3_cross(cam->y, cam->z, cam->x)); NCHECK(f, 0);
  f3_divf(cam->z, cam->z, (float)tan(fov_x*0.5f));
  f3_divf(cam->y, cam->y, proj_ratio);
}

static void
camera_ray
  (const struct camera* cam,
   const float pixel[2],
   float org[3],
   float dir[3])
{
  float x[3], y[3], f;
  ASSERT(cam && pixel && org && dir);

  f3_mulf(x, cam->x, pixel[0]*2.f - 1.f);
  f3_mulf(y, cam->y, pixel[1]*2.f - 1.f);
  f3_add(dir, f3_add(dir, x, y), cam->z);
  f = f3_normalize(dir, dir); NCHECK(f, 0);
  f3_set(org, cam->pos);
}

int
main(int argc, char** argv)
{
  struct mem_allocator allocator;
  struct s3d_device* dev;
  struct s3d_hit hit;
  struct s3d_scene* scn;
  struct s3d_shape* parallelepiped;
  struct camera cam;
  unsigned char* img = NULL;
  size_t ix, iy;
  float org[3] = { 0.f, 0.f, 0.f };
  float dir[3] = { 0.f, 1.f, 0.f };
  float range[2] = { 0.f, FLT_MAX };
  struct meshed_parallelepiped_options_T options;

  float edge1[3] = { 300.f, 20.f, 20.f };
  float edge2[3] = { 20.f, 320.f, 20.f };
  float edge3[3] = { 20.f, 20.f, 420.f };
  float C[3] = { 150.f, 175.f, 200.f };

  mem_init_proxy_allocator(&allocator, &mem_default_allocator);

  if(argc > 1) {
    img = MEM_ALLOC(&allocator, 3 * IMG_WIDTH * IMG_HEIGHT);
    NCHECK(img, NULL);
  }

  options = default_meshed_parallelepiped_options;
  options.output_final_mesh = 1;
  options.mesh_orientation = CCW;
  f3_set(options.center, C);

  CHECK(s3d_device_create(NULL, &allocator, 0, &dev), RES_OK);
  CHECK(s3d_scene_create(dev, &scn), RES_OK);
  CHECK(s3du_shape_create_meshed_parallelepiped
    (dev, &parallelepiped, edge1, edge2, edge3, &options), RES_OK);
  CHECK(s3d_scene_attach_shape(scn, parallelepiped), RES_OK);
  CHECK(s3d_scene_begin_session(scn, S3D_TRACE), RES_OK);
  
  camera_init(&cam);
  FOR_EACH(iy, 0, IMG_HEIGHT) {
    float pixel[2];

    pixel[1] = (float)iy/(float)IMG_HEIGHT;
    FOR_EACH(ix, 0, IMG_WIDTH) {
      const size_t ipix = (iy*IMG_WIDTH + ix) * 3/*RGB*/;

      pixel[0] = (float)ix/(float)IMG_WIDTH;
      camera_ray(&cam, pixel, org, dir);
      CHECK(s3d_scene_trace_ray(scn, org, dir, range, NULL, &hit), RES_OK);

      if(S3D_HIT_NONE(&hit)) {
        if(img) {
          img[ipix+0] = img[ipix+1] = img[ipix+2] = 0;
        }
      } else {
        float N[3], len, w;
        struct s3d_attrib attr;
        float pos[3];

        CHECK(s3d_primitive_get_attrib
          (&hit.prim, S3D_POSITION, hit.uv, &attr), RES_OK);
        CHECK(attr.type, S3D_FLOAT3);
        CHECK(attr.usage, S3D_POSITION);
        f3_add(pos, f3_mulf(pos, dir, hit.distance), org);
        len = f3_normalize(N, hit.normal);
        NCHECK(len, 0);

        CHECK(s3d_primitive_get_attrib
          (&hit.prim, S3D_GEOMETRY_NORMAL, hit.uv, &attr), RES_OK);
        CHECK(attr.type, S3D_FLOAT3);
        CHECK(attr.usage, S3D_GEOMETRY_NORMAL);
        f3_normalize(attr.value, attr.value);
        CHECK(f3_eq_eps(attr.value, N, 1.e-6f), 1);

        if(!img)
          continue;

        w = CLAMP(1.f - hit.uv[0] - hit.uv[1], 0.f, 1.f);
        img[ipix+0] = (unsigned char)(hit.uv[0] * 255.f);
        img[ipix+1] = (unsigned char)(hit.uv[1] * 255.f);
        img[ipix+2] = (unsigned char)(w * 255.f);
      }
    }
  }
  CHECK(s3d_scene_end_session(scn), RES_OK);

  if(argc > 1) {
    CHECK(image_ppm_write(argv[1], IMG_WIDTH, IMG_HEIGHT, 3, img), RES_OK);
  }

  if(img)
    MEM_RM(&allocator, img);

  CHECK(s3d_shape_ref_put(parallelepiped), RES_OK);
  CHECK(s3d_device_ref_put(dev), RES_OK);
  CHECK(s3d_scene_ref_put(scn), RES_OK);

  check_memory_allocator(&allocator);
  mem_shutdown_proxy_allocator(&allocator);
  CHECK(mem_allocated_size(), 0);

  return 0;
}
